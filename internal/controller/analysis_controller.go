// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

// SECURITY: This workflow executes untrusted code
// The following measures must be employed for untrusted pods:
//   1. No (=> default) service account (#security.service_account)
//   2. label 'security.dyff.io/untrusted: "true"' is applied
//      => deny-all NetworkPolicy applies (#security.untrusted_label)
//      FIXME: Should use whitelist (security.dyff.io/trusted) instead
//   3. run in gVisor (#security.gvisor)
//   4. Set a timeout on the job (#security.timeout)

package controller

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/cli-runtime/pkg/printers"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
)

// AnalysisReconciler reconciles a Analysis object
type AnalysisReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	ownerKey string
}

func AnalysisWorkflowPlural() string {
	return "analyses"
}

func AnalysisWorkflowSingleLetter() string {
	return "a"
}

func (r *AnalysisReconciler) StatusDownloadSuccessful() string {
	return "analyses.dyff.io/DownloadSuccessful"
}
func (r *AnalysisReconciler) StatusRunSuccessful() string {
	return "analyses.dyff.io/RunSuccessful"
}
func (r *AnalysisReconciler) StatusUploadSuccessful() string {
	return "analyses.dyff.io/UploadSuccessful"
}

// Returns the name that should be assigned to a child k8s resource according
// to which component of the Analysis workflow the child resource belongs to.
func (r *AnalysisReconciler) componentName(analysis *dyffv1alpha1.Analysis, component kinds.WorkflowComponent) string {
	id := lastIdComponent(string(analysis.Spec.Id))
	return fmt.Sprintf("%v-%v", id, component.SingleLetterComponent)
}

func (r *AnalysisReconciler) pvcLabels(analysis *dyffv1alpha1.Analysis) map[string]string {
	return map[string]string{
		"dyff.io/account":   analysis.Spec.Account,
		"dyff.io/workflow":  AnalysisWorkflowPlural(),
		"dyff.io/component": "pvc",
		"dyff.io/id":        lastIdComponent(string(analysis.Spec.Id)),
	}
}

func (r *AnalysisReconciler) downloadLabels(analysis *dyffv1alpha1.Analysis) map[string]string {
	return map[string]string{
		"dyff.io/account":   analysis.Spec.Account,
		"dyff.io/workflow":  AnalysisWorkflowPlural(),
		"dyff.io/step":      kinds.AnalysisDownload().Component,
		"dyff.io/component": kinds.AnalysisDownload().Component,
		"dyff.io/id":        lastIdComponent(string(analysis.Spec.Id)),
	}
}

func (r *AnalysisReconciler) uploadLabels(analysis *dyffv1alpha1.Analysis) map[string]string {
	return map[string]string{
		"dyff.io/account":   analysis.Spec.Account,
		"dyff.io/workflow":  AnalysisWorkflowPlural(),
		"dyff.io/step":      kinds.AnalysisUpload().Component,
		"dyff.io/component": kinds.AnalysisUpload().Component,
		"dyff.io/id":        lastIdComponent(string(analysis.Spec.Id)),
	}
}

func (r *AnalysisReconciler) runLabels(analysis *dyffv1alpha1.Analysis) map[string]string {
	// SECURITY: This pod executes untrusted code
	// See comment at top of file
	return map[string]string{
		"dyff.io/account":   analysis.Spec.Account,
		"dyff.io/workflow":  AnalysisWorkflowPlural(),
		"dyff.io/step":      kinds.AnalysisRun().Component,
		"dyff.io/component": kinds.AnalysisRun().Component,
		"dyff.io/id":        lastIdComponent(string(analysis.Spec.Id)),
		// FIXME: Should use a whitelist rather than a blacklist
		// i.e., default is "untrusted", must mark things as "trusted"
		"security.dyff.io/untrusted": "true", // #security.untrusted_label
	}
}

// Returns the .metadata.labels for the ConfigMap resource.
func (r *AnalysisReconciler) configLabels(analysis *dyffv1alpha1.Analysis, component kinds.WorkflowComponent) map[string]string {
	return map[string]string{
		"dyff.io/account":   analysis.Spec.Account,
		"dyff.io/workflow":  AnalysisWorkflowPlural(),
		"dyff.io/step":      component.ShortComponent,
		"dyff.io/component": "config",
		"dyff.io/id":        lastIdComponent(string(analysis.Spec.Id)),
	}
}

func (r *AnalysisReconciler) createTemporaryPVC(analysis *dyffv1alpha1.Analysis) *corev1.PersistentVolumeClaim {
	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.pvcLabels(analysis),
			Name:      r.componentName(analysis, kinds.AnalysisPVC()),
			Namespace: analysis.Namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					// TODO: Make configurable
					"storage": resource.MustParse("10Gi"),
				},
			},
		},
	}
}

func (r *AnalysisReconciler) pvcMountPath() string {
	return "/dyff/mnt"
}

func (r *AnalysisReconciler) artifactLocalPath(id dyffv1alpha1.EntityID) string {
	return fmt.Sprintf("%v/%v", r.pvcMountPath(), id)
}

func (r *AnalysisReconciler) createDownloadJob(analysis *dyffv1alpha1.Analysis) *batchv1.Job {
	pvcName := r.componentName(analysis, kinds.AnalysisPVC())

	inputKinds := make(map[string]string)
	for _, input := range analysis.Spec.Method.Inputs {
		inputKinds[input.Keyword] = input.Kind
	}

	var transfers []string

	// Input datasets
	for _, input := range analysis.Spec.Inputs {
		storagePath := artifactStorageUrl(inputKinds[input.Keyword], input.Entity)
		localPath := r.artifactLocalPath(input.Entity)
		transfers = append(transfers, fmt.Sprintf("%v>%v", storagePath, localPath))
	}

	// Extension modules needed by the Analysis
	for _, module := range analysis.Spec.Method.Modules {
		modulePath := artifactStorageUrl("Module", module)
		moduleTransfer := fmt.Sprintf("%v>%v", modulePath, r.artifactLocalPath(module))
		transfers = append(transfers, moduleTransfer)
	}

	// Command-line arguments
	args := make([]string, len(transfers))
	for i := range transfers {
		args[i] = fmt.Sprintf("--transfer=%v", transfers[i])
	}

	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.downloadLabels(analysis),
			Name:      r.componentName(analysis, kinds.AnalysisDownload()),
			Namespace: analysis.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.downloadLabels(analysis),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  r.componentName(analysis, kinds.AnalysisDownload()),
							Image: os.Getenv("DYFF_WORKFLOWS__COMMON__STORAGE_TRANSFER__IMAGE"),
							Args:  args,
							EnvFrom: []corev1.EnvFromSource{
								{
									ConfigMapRef: &corev1.ConfigMapEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-config",
										},
									},
								},
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-storage-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: r.pvcMountPath(),
								},
							},
						},
					},
					RestartPolicy:   "Never",
					SecurityContext: workflowsPodSecurityContext(),
					// TODO: Rename to analysis-runner
					ServiceAccountName: "report-runner",
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
					},
				},
			},
		},
	}
}

func (r *AnalysisReconciler) createUploadJob(analysis *dyffv1alpha1.Analysis) *batchv1.Job {
	pvcName := r.componentName(analysis, kinds.AnalysisPVC())

	localPath := r.artifactLocalPath(analysis.Spec.Id)
	storagePath := artifactStorageUrl(analysis.Spec.Method.Output.Kind, analysis.Spec.Id)
	transfer := fmt.Sprintf("%v>%v", localPath, storagePath)

	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.uploadLabels(analysis),
			Name:      r.componentName(analysis, kinds.AnalysisUpload()),
			Namespace: analysis.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.uploadLabels(analysis),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  r.componentName(analysis, kinds.AnalysisUpload()),
							Image: os.Getenv("DYFF_WORKFLOWS__COMMON__STORAGE_TRANSFER__IMAGE"),
							// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/storage-transfer:latest",
							Args: []string{
								fmt.Sprintf("--transfer=%v", transfer),
							},
							EnvFrom: []corev1.EnvFromSource{
								{
									ConfigMapRef: &corev1.ConfigMapEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-config",
										},
									},
								},
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-storage-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: r.pvcMountPath(),
								},
							},
						},
					},
					RestartPolicy:   "Never",
					SecurityContext: workflowsPodSecurityContext(),
					// TODO: Rename to analysis-runner
					ServiceAccountName: "report-runner",
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
					},
				},
			},
		},
	}
}

// Create the ConfigMap containing the Analysis spec
func (r *AnalysisReconciler) createConfigMap(analysis *dyffv1alpha1.Analysis, component kinds.WorkflowComponent) *corev1.ConfigMap {
	var builder strings.Builder
	yml := printers.YAMLPrinter{}
	if err := yml.PrintObj(analysis, &builder); err != nil {
		return nil
	}

	name := r.componentName(analysis, component)
	immutable := true
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.configLabels(analysis, component),
			Name:      name,
			Namespace: analysis.Namespace,
		},
		Immutable: &immutable,
		Data: map[string]string{
			"analysis.yaml": builder.String(),
		},
	}
}

func (r *AnalysisReconciler) createRunJob(analysis *dyffv1alpha1.Analysis) *batchv1.Job {
	// SECURITY: This pod executes untrusted code
	// See comment at top of file
	pvcName := r.componentName(analysis, kinds.AnalysisPVC())
	tmpName := r.componentName(analysis, kinds.AnalysisTmpVolume())
	const tmpPath string = "/tmp"

	var backoffLimit int32 = 1

	var runtimeClassNamePointer *string
	// TODO: Rename this env var to DYFF_WORKFLOWS__UNTRUSTED__... or something
	// We might use the address of the value, so be careful about aliasing
	runtimeClassName, ok := os.LookupEnv("DYFF_WORKFLOWS__REPORTS__RUN__RUNTIME_CLASS_NAME")
	if ok {
		runtimeClassNamePointer = &runtimeClassName // #security.gvisor
	}

	var activeDeadlineSeconds int64 = 600 // #security.timeout
	activeDeadlineSecondsValue, ok := os.LookupEnv("DYFF_WORKFLOWS__REPORTS__RUN__ACTIVE_DEADLINE_SECONDS")
	if ok {
		seconds, err := strconv.ParseInt(activeDeadlineSecondsValue, 10, 64)
		if err == nil {
			activeDeadlineSeconds = seconds
		}
	}

	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.runLabels(analysis),
			Name:      r.componentName(analysis, kinds.AnalysisRun()),
			Namespace: analysis.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			// Do not specify a service account #security.service_account
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.runLabels(analysis),
				},
				Spec: corev1.PodSpec{
					RuntimeClassName:      runtimeClassNamePointer,
					ActiveDeadlineSeconds: &activeDeadlineSeconds,
					Containers: []corev1.Container{
						{
							Name: r.componentName(analysis, kinds.AnalysisRun()),
							// TODO: Rename env var to __ANALYSES__
							Image: os.Getenv("DYFF_WORKFLOWS__REPORTS__RUN__IMAGE"),
							Env: []corev1.EnvVar{
								{
									Name:  "DYFF_AUDIT_ANALYSIS_CONFIG_FILE",
									Value: "/etc/config/analysis.yaml",
								},
								{
									Name:  "DYFF_AUDIT_LOCAL_STORAGE_ROOT",
									Value: r.pvcMountPath(),
								},
								{
									Name:  "HOME",
									Value: tmpPath,
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("8000m"),
									"memory": resource.MustParse("16Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("8000m"),
									"memory": resource.MustParse("16Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: r.pvcMountPath(),
								},
								{
									Name:      "config",
									MountPath: "/etc/config",
								},
								{
									Name:      tmpName,
									MountPath: tmpPath,
								},
							},
						},
					},
					SecurityContext: workflowsPodSecurityContext(),
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
						{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: r.componentName(analysis, kinds.AnalysisRun()),
									},
								},
							},
						},
						{
							Name: tmpName,
							VolumeSource: corev1.VolumeSource{
								Ephemeral: &corev1.EphemeralVolumeSource{
									VolumeClaimTemplate: &corev1.PersistentVolumeClaimTemplate{
										Spec: corev1.PersistentVolumeClaimSpec{
											AccessModes: []corev1.PersistentVolumeAccessMode{"ReadWriteOnce"},
											Resources: corev1.ResourceRequirements{
												Requests: corev1.ResourceList{
													"storage": resource.MustParse("16Gi"),
												},
											},
											// TODO: GKE-specific name
											// StorageClassName: Pointer("standard-rwo"),
										},
									},
								},
							},
						},
					},
					RestartPolicy: "Never",
				},
			},
		},
	}
}

// If the workflow has reached a terminal Condition, return that Condition. The
// operator should clean up any resources and set the returned Condition in the
// workflow k8s manifest.
//
// Else, return nil. The operator should let the workflow continue running.
func (r *AnalysisReconciler) terminalCondition(conditions []metav1.Condition) *metav1.Condition {
	var rootCause *metav1.Condition
	var condition *metav1.Condition

	// If top-level terminal condition is already set, return it
	for _, conditionType := range []string{StatusComplete, StatusFailed} {
		condition = k8sMeta.FindStatusCondition(conditions, conditionType)
		if condition != nil && condition.Status == metav1.ConditionTrue {
			return condition
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, r.StatusDownloadSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	} else if condition.Status == metav1.ConditionFalse {
		// Fail immediately if Download failed
		return &metav1.Condition{
			Type:               StatusFailed,
			Status:             metav1.ConditionTrue,
			Reason:             "DownloadStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Download step Failed.",
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, r.StatusRunSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	} else if condition.Status == metav1.ConditionFalse {
		// If Run failed, we still want to Upload the log data [*]
		rootCause = &metav1.Condition{
			Type:               StatusFailed,
			Status:             metav1.ConditionTrue,
			Reason:             "RunStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Run step Failed.",
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, r.StatusUploadSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	}

	// [*] Now we can report Failed
	// Even if Upload failed, too, it's more important that the user know that
	// the Run step failed, and we can only report one status.
	if rootCause != nil {
		return rootCause
	}

	if condition.Status == metav1.ConditionFalse {
		// Fail immediately if Upload failed
		return &metav1.Condition{
			Type:               StatusFailed,
			Status:             metav1.ConditionTrue,
			Reason:             "UploadStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Upload step Failed.",
		}
	}

	// Everything succeeded
	return &metav1.Condition{
		Type:               StatusComplete,
		Status:             metav1.ConditionTrue,
		Reason:             StatusComplete,
		LastTransitionTime: metav1.NewTime(time.Now()),
		Message:            "All steps are Complete.",
	}
}

//+kubebuilder:rbac:groups=dyff.io,resources=analyses,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=analyses/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=analyses/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=configmaps;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Analysis object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *AnalysisReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var err error
	var logger = log.FromContext(ctx)

	// Get the Analysis
	var analysis dyffv1alpha1.Analysis
	err = r.Get(ctx, req.NamespacedName, &analysis)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("Analysis NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get Analysis")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// If in a terminal condition, set condition, clean up resources, return
	if terminal := r.terminalCondition(analysis.Status.Conditions); terminal != nil {
		logger.Info("terminal status: ", "status", terminal)

		k8sMeta.SetStatusCondition(&analysis.Status.Conditions, *terminal)
		err = r.Status().Update(ctx, &analysis)
		if err != nil {
			return ctrl.Result{}, err
		}

		// Only delete the Jobs after Condition updates are visible, since we
		// use Job status as a cue for which step of the workflow we're in.
		err = r.Delete(ctx, r.createDownloadJob(&analysis), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
		err = r.Delete(ctx, r.createRunJob(&analysis), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
		err = r.Delete(ctx, r.createUploadJob(&analysis), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		// Shared dependencies
		err = r.Delete(ctx, r.createTemporaryPVC(&analysis))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
		err = r.Delete(ctx, r.createConfigMap(&analysis, kinds.AnalysisRun()))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	// Get child Jobs
	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	var downloadFailed bool = false
	var runFailed bool = false
	var uploadFailed bool = false
	for _, job := range failedJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.AnalysisDownload().Component {
			downloadFailed = true
		} else if step == kinds.AnalysisRun().Component {
			runFailed = true
		} else if step == kinds.AnalysisUpload().Component {
			uploadFailed = true
		}
	}

	var downloadSucceeded bool = false
	var runSucceeded bool = false
	var uploadSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.AnalysisDownload().Component {
			downloadSucceeded = true
		} else if step == kinds.AnalysisRun().Component {
			runSucceeded = true
		} else if step == kinds.AnalysisUpload().Component {
			uploadSucceeded = true
		}
	}

	// No pending jobs. Update status and start next step.
	if uploadSucceeded || uploadFailed {
		if uploadSucceeded {
			k8sMeta.SetStatusCondition(
				&analysis.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusUploadSuccessful(),
					Status:             metav1.ConditionTrue,
					Reason:             "UploadStepComplete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Upload step is Complete.",
				},
			)
			err = r.Status().Update(ctx, &analysis)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("analysis workflow succeeded")
		} else if uploadFailed {
			k8sMeta.SetStatusCondition(
				&analysis.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusUploadSuccessful(),
					Status:             metav1.ConditionFalse,
					Reason:             "UploadStepFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Upload step Failed.",
				},
			)
			err = r.Status().Update(ctx, &analysis)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("analysis workflow failed")
		}
	} else if runSucceeded || runFailed {
		if runSucceeded {
			k8sMeta.SetStatusCondition(
				&analysis.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusRunSuccessful(),
					Status:             metav1.ConditionTrue,
					Reason:             "RunStepComplete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Run step is Complete.",
				},
			)
			err = r.Status().Update(ctx, &analysis)
			if err != nil {
				return ctrl.Result{}, err
			}
		} else if runFailed {
			k8sMeta.SetStatusCondition(
				&analysis.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusRunSuccessful(),
					Status:             metav1.ConditionFalse,
					Reason:             "RunStepFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Run step Failed.",
				},
			)
			err = r.Status().Update(ctx, &analysis)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("analysis workflow failed")
		}

		// Next step is Upload, even if Run failed, so that we can preserve logs
		uploadJob := r.createUploadJob(&analysis)
		if err := ctrl.SetControllerReference(&analysis, uploadJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt upload Job")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, uploadJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create upload Job", "uploadJob", uploadJob)
			return ctrl.Result{}, err
		}

		logger.Info("created workflow step: analysis.upload")
	} else if downloadSucceeded || downloadFailed {
		if downloadSucceeded {
			k8sMeta.SetStatusCondition(
				&analysis.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusDownloadSuccessful(),
					Status:             metav1.ConditionTrue,
					Reason:             "DownloadStepComplete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Download step is Complete.",
				},
			)
			err = r.Status().Update(ctx, &analysis)
			if err != nil {
				return ctrl.Result{}, err
			}

			// Next step is Run
			runJob := r.createRunJob(&analysis)
			if err := ctrl.SetControllerReference(&analysis, runJob, r.Scheme); err != nil {
				logger.Error(err, "failed to adopt run Job")
				return ctrl.Result{}, err
			}
			err = r.Create(ctx, runJob)
			if client.IgnoreAlreadyExists(err) != nil {
				logger.Error(err, "failed to create run Job", "runJob", runJob)
				return ctrl.Result{}, err
			}
			logger.Info("created workflow step: analysis.run")
		} else if downloadFailed {
			k8sMeta.SetStatusCondition(
				&analysis.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusDownloadSuccessful(),
					Status:             metav1.ConditionFalse,
					Reason:             "DownloadStepFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Download step Failed.",
				},
			)
			err = r.Status().Update(ctx, &analysis)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("analysis workflow failed")
		}
	} else {
		// Nothing has run yet

		// Create shared dependencies
		pvc := r.createTemporaryPVC(&analysis)
		if err := ctrl.SetControllerReference(&analysis, pvc, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt shared PVC")
			return ctrl.Result{}, err
		}
		config := r.createConfigMap(&analysis, kinds.AnalysisRun())
		if err := ctrl.SetControllerReference(&analysis, config, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt analysis ConfigMap")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, pvc)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create shared PVC", "pvc", pvc)
			return ctrl.Result{}, err
		}
		err = r.Create(ctx, config)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create analysis ConfigMap", "config", config)
			return ctrl.Result{}, err
		}

		// First step is Download
		downloadJob := r.createDownloadJob(&analysis)
		if err := ctrl.SetControllerReference(&analysis, downloadJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt download Job")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, downloadJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create download Job", "downloadJob", downloadJob)
			return ctrl.Result{}, err
		}

		logger.Info("created workflow step: analysis.download")

		k8sMeta.SetStatusCondition(
			&analysis.Status.Conditions,
			metav1.Condition{
				Type:               StatusComplete,
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Analysis is running.",
			},
		)
		k8sMeta.SetStatusCondition(
			&analysis.Status.Conditions,
			metav1.Condition{
				Type:               StatusFailed,
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Analysis is running.",
			},
		)
		if err := r.Status().Update(ctx, &analysis); err != nil {
			return ctrl.Result{}, err
		}
	}

	// Return OK
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *AnalysisReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.Analysis)

	// Cache Jobs whose owner is an Analysis
	err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.Analysis {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": AnalysisWorkflowPlural(),
		},
	})
	if err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.Analysis{}).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.PersistentVolumeClaim{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.ConfigMap{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
