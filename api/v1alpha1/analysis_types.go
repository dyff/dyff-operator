// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Measurement struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Name string `json:"name"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=Instance;Dataset
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Level string `json:"level"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Schema DataSchema `json:"schema"`
}

type SafetyCase struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Name string `json:"name"`
}

// An input Arrow dataset for the Method. The data will be exposed to the
// analysis method under a specified keyword.
type MethodInput struct {
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=Dataset;Evaluation;Measurement;Report
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// The data will be exposed to the Analysis.method code under this name,
	// i.e., as a keyword parameter. The name must be unique among inputs to the Method.
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Keyword string `json:"keyword"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Adapter []SchemaAdapter `json:"adapter,omitempty"`
}

// The output of the analysis. The output Arrow dataset will be stored at the
// configured location for the given resource group and resource ID.
type MethodOutput struct {
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=Measurement;SafetyCase
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Measurement *Measurement `json:"measurement,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	SafetyCase *SafetyCase `json:"safetyCase,omitempty"`
}

type MethodImplementationJupyterNotebook struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	NotebookModule string `json:"notebookModule"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	NotebookPath string `json:"notebookPath"`
}

type MethodImplementationPythonFunction struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	FullyQualifiedName string `json:"fullyQualifiedName"`
}

type MethodImplementationPythonRubric struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	FullyQualifiedName string `json:"fullyQualifiedName"`
}

type MethodImplementation struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	JupyterNotebook *MethodImplementationJupyterNotebook `json:"jupyterNotebook,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	PythonFunction *MethodImplementationPythonFunction `json:"pythonFunction,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	PythonRubric *MethodImplementationPythonRubric `json:"pythonRubric,omitempty"`
}

type MethodParameter struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Keyword string `json:"keyword"`
}

type ScoreSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Name string `json:"name"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Title string `json:"title"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Summary string `json:"summary"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Valence string `json:"valence"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Priority string `json:"priority"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Minimum *string `json:"minimum,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Maximum *string `json:"maximum,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Format *string `json:"format,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Unit *string `json:"unit,omitempty"`
}

type Method struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Id EntityID `json:"id"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Name string `json:"name"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum=Evaluation;InferenceService
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Scope string `json:"scope"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Implementation MethodImplementation `json:"implementation"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Parameters []MethodParameter `json:"parameters,omitempty"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinItems=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Inputs []MethodInput `json:"inputs"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Output MethodOutput `json:"output"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Scores []ScoreSpec `json:"scores,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Modules []EntityID `json:"modules,omitempty"`
}

type AnalysisArgument struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Keyword string `json:"keyword"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Value string `json:"value"`
}

type AnalysisInputMapping struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Keyword string `json:"keyword"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Entity EntityID `json:"entity"`
}

type AnalysisScope struct {
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Dataset *string `json:"dataset,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Evaluation *string `json:"evaluation,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	InferenceService *string `json:"inferenceService,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Model *string `json:"model,omitempty"`
}

// Arbitrary additional data for the Analysis, specified as a key-value pair
// where the value is the data encoded in base64.
type AnalysisData struct {
	// Key identifying the data. For data about a Dyff entity, this should
	// be the entity's ID.
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Key string `json:"key"`

	// Arbitrary data encoded as a string.
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Value string `json:"value"`
}

// AnalysisSpec defines the desired state of Analysis
type AnalysisSpec struct {
	DyffEntity `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Method Method `json:"method"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Scope *AnalysisScope `json:"scope,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Arguments []AnalysisArgument `json:"arguments,omitempty"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinItems=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Inputs []AnalysisInputMapping `json:"inputs"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Data []AnalysisData `json:"data,omitempty"`
}

// AnalysisStatus defines the observed state of Analysis
type AnalysisStatus struct {
	// +operator-sdk:csv:customresourcedefinitions:type=status
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Analysis is the Schema for the analysis API
// +kubebuilder:resource:path=analyses
type Analysis struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   AnalysisSpec   `json:"spec,omitempty"`
	Status AnalysisStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// AnalysisList contains a list of Analysis
type AnalysisList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Analysis `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Analysis{}, &AnalysisList{})
}
