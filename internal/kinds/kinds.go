// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package kinds

type WorkflowComponent struct {
	Kind                  string
	ShortName             string
	Component             string
	ShortComponent        string
	SingleLetterComponent string
}

const (
	Analysis         string = "Analysis"
	Audit            string = "Audit"
	Dataset          string = "Dataset"
	Evaluation       string = "Evaluation"
	InferenceService string = "InferenceService"
	InferenceSession string = "InferenceSession"
	Model            string = "Model"
	Report           string = "Report"
)

const (
	AnalysisShort         string = "analysis"
	AuditShort            string = "audit"
	DatasetShort          string = "data"
	EvaluationShort       string = "eval"
	InferenceServiceShort string = "infer"
	InferenceSessionShort string = "session"
	ModelShort            string = "model"
	ReportShort           string = "report"
)

func AnalysisDownload() WorkflowComponent {
	return WorkflowComponent{Analysis, AnalysisShort, "download", "down", "d"}
}

func AnalysisRun() WorkflowComponent {
	return WorkflowComponent{Analysis, AnalysisShort, "run", "run", "r"}
}

func AnalysisUpload() WorkflowComponent {
	return WorkflowComponent{Analysis, AnalysisShort, "upload", "up", "u"}
}

func AnalysisPVC() WorkflowComponent {
	return WorkflowComponent{Analysis, AnalysisShort, "pvc", "pvc", "v"}
}

func AnalysisTmpVolume() WorkflowComponent {
	return WorkflowComponent{Analysis, AnalysisShort, "tmp", "tmp", "t"}
}

func AuditFetch() WorkflowComponent {
	return WorkflowComponent{Audit, AuditShort, "fetch", "fetch", "f"}
}
func AuditProxy() WorkflowComponent {
	return WorkflowComponent{Audit, AuditShort, "proxy", "proxy", "p"}
}
func AuditRun() WorkflowComponent {
	return WorkflowComponent{Audit, AuditShort, "run", "run", "r"}
}

func DatasetIngest() WorkflowComponent {
	return WorkflowComponent{Dataset, DatasetShort, "ingest", "ingest", "i"}
}

func EvaluationClient() WorkflowComponent {
	return WorkflowComponent{Evaluation, EvaluationShort, "client", "client", "c"}
}
func EvaluationInference() WorkflowComponent {
	return WorkflowComponent{Evaluation, EvaluationShort, "inference", "infer", "i"}
}
func EvaluationVerification() WorkflowComponent {
	return WorkflowComponent{Evaluation, EvaluationShort, "verification", "verify", "v"}
}

func InferenceServiceBuild() WorkflowComponent {
	return WorkflowComponent{InferenceService, InferenceServiceShort, "build", "build", "b"}
}

func InferenceSessionConfig() WorkflowComponent {
	return WorkflowComponent{InferenceSession, InferenceSessionShort, "config", "config", "c"}
}

func InferenceSessionDownload() WorkflowComponent {
	return WorkflowComponent{InferenceSession, InferenceSessionShort, "download", "down", "d"}
}

func InferenceSessionRun() WorkflowComponent {
	return WorkflowComponent{InferenceSession, InferenceSessionShort, "run", "run", "r"}
}

func InferenceSessionInference() WorkflowComponent {
	return WorkflowComponent{InferenceSession, InferenceSessionShort, "inference", "infer", "i"}
}

func ModelFetch() WorkflowComponent {
	return WorkflowComponent{Model, ModelShort, "fetch", "fetch", "f"}
}

func ModelConfig() WorkflowComponent {
	return WorkflowComponent{Model, ModelShort, "config", "config", "c"}
}

func ModelArtifacts() WorkflowComponent {
	return WorkflowComponent{Model, ModelShort, "artifacts", "artifacts", "a"}
}

func ModelStorage() WorkflowComponent {
	return WorkflowComponent{Model, ModelShort, "storage", "storage", "s"}
}

func ModelSnapshot() WorkflowComponent {
	return WorkflowComponent{Model, ModelShort, "snapshot", "snapshot", "t"}
}

func ModelReadOnlyManyVolume() WorkflowComponent {
	return WorkflowComponent{Model, ModelShort, "rox", "rox", "x"}
}

func ReportDownload() WorkflowComponent {
	return WorkflowComponent{Report, ReportShort, "download", "down", "d"}
}

func ReportRun() WorkflowComponent {
	return WorkflowComponent{Report, ReportShort, "run", "run", "r"}
}

func ReportUpload() WorkflowComponent {
	return WorkflowComponent{Report, ReportShort, "upload", "up", "u"}
}

func ReportPVC() WorkflowComponent {
	return WorkflowComponent{Report, ReportShort, "pvc", "pvc", "v"}
}

func ReportTmpVolume() WorkflowComponent {
	return WorkflowComponent{Report, ReportShort, "tmp", "tmp", "t"}
}
