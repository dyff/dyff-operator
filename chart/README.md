# dyff-operator

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/dyff-operator)](https://artifacthub.io/packages/search?repo=dyff-operator)

Operator to manage deployments of Dyff CRDs.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Installation

```bash
helm install dyff-operator oci://registry.gitlab.com/dyff/charts/dyff-operator
```

## Removal

Now the chart can be deleted:

```bash
helm uninstall dyff-operator
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Set affinity rules. |
| args | list | `["--leader-election-id","1.dyff.io"]` | Arguments to pass to the dyff-operator container. |
| command | list | `["/dyff-operator"]` | Command to run for the dyff-operator container. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` |  |
| containerSecurityContext.capabilities.drop[0] | string | `"ALL"` |  |
| containerSecurityContext.privileged | bool | `false` |  |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` |  |
| containerSecurityContext.runAsGroup | int | `1001` |  |
| containerSecurityContext.runAsNonRoot | bool | `true` |  |
| containerSecurityContext.runAsUser | int | `1001` |  |
| crds.enabled | bool | `true` | Enable CRDs. |
| extraEnvVarsConfigMap | object | `{}` | Set environment variables in the `dyff-operator` Deployment via ConfigMap. |
| extraEnvVarsSecret | object | `{}` | Set environment variables in the `dyff-operator` Deployment via Secret. |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` | Set the pull policy. |
| image.repository | string | `"registry.gitlab.com/dyff/dyff-operator"` | Set the repository to pull an image from. |
| image.tag | string | `""` | Override the image tag whose default is the chart appVersion. |
| image.workflows.evaluationClient | string | `"registry.gitlab.com/dyff/workflows/evaluation-client:0.4.7"` | Set evaluations client image |
| image.workflows.fetchModel | string | `"registry.gitlab.com/dyff/workflows/fetch-model:0.1.3"` | Set fetch model image |
| image.workflows.runReport | string | `"registry.gitlab.com/dyff/workflows/run-report:0.7.5"` | Set reports run image |
| image.workflows.storageTransfer | string | `"registry.gitlab.com/dyff/workflows/storage-transfer:0.3.6"` | Set storage transfer image |
| image.workflows.verifyEvaluationOutput | string | `"registry.gitlab.com/dyff/workflows/verify-evaluation-output:0.2.2"` | Set evaluations verification image |
| imagePullSecrets | list | `[]` | Set image to pull from private registry. |
| livenessProbe.httpGet.path | string | `"/healthz"` | Set livenessProbe path. |
| livenessProbe.httpGet.port | int | `8081` | Set livenessProbe port. |
| livenessProbe.initialDelaySeconds | int | `15` | Set how soon livenessProbe should wait before performing first probe. |
| livenessProbe.periodSeconds | int | `20` | Set how often the livenessProbe should check. |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` | Set the node labels you want the target node to have. |
| podAnnotations | object | `{}` | Set annotations for pods. |
| podLabels | object | `{}` | Set labels for pods. |
| podSecurityContext.fsGroup | int | `1001` | Define the filesystem group ID that owns the volume's files and directories. |
| readinessProbe.httpGet.path | string | `"/readyz"` | Set readinessProbe path. |
| readinessProbe.httpGet.port | int | `8081` | Set readinessProbe port. |
| readinessProbe.initialDelaySeconds | int | `5` | Set how soon readinessProbe should wait before performing first probe. |
| readinessProbe.periodSeconds | int | `10` | Set how often the readinessProbe should check. |
| replicaCount | int | `1` | Set the number of replicas to deploy. |
| reports.activeDeadlineSeconds | int | `86400` | Set amount of time job will be available before shutting down. |
| reports.runtimeClassName | string | `nil` | Set runtimeClassName. If you want an alternative runtime class, uncomment the runtimeClassName and set it to the runtime class of your choice. |
| resources | object | `{}` | Set container requests and limits for different resources like CPU or memory (essential for production workloads). |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account. |
| serviceAccount.automount | bool | `true` | Chose to automatically mount a ServiceAccount's API credentials. |
| serviceAccount.create | bool | `true` | Specify if a service account should be created. |
| serviceAccount.name | string | `""` | If not set and create is true, a name is generated using the fullname template. |
| storage.backend | string | `"s3"` |  |
| storage.s3.accessKey | string | `""` | S3 access key. |
| storage.s3.backendClass | string | `"dyff.storage.backend.s3.storage.S3StorageBackend"` | S3 storage backend class to use. |
| storage.s3.endpoint | string | `""` | External S3 endpoint URL reachable from outside the network. |
| storage.s3.secretKey | string | `""` | S3 secret key. |
| storage.urls.datasets | string | `"s3://dyff/datasets"` | Bucket URL where datasets will be stored. |
| storage.urls.measurements | string | `"s3://dyff/measurements"` | Bucket URL where measurements will be stored. |
| storage.urls.models | string | `"s3://dyff/models"` | Bucket URL where models will be stored. |
| storage.urls.modules | string | `"s3://dyff/modules"` | Bucket URL where modules will be stored. |
| storage.urls.outputs | string | `"s3://dyff/outputs"` | Bucket URL where outputs will be stored. |
| storage.urls.reports | string | `"s3://dyff/reports"` | Bucket URL where reports will be stored. |
| storage.urls.safetycases | string | `"s3://dyff/safetycases"` | Bucket URL where safetycases will be stored. |
| tolerations | list | `[]` | Set the scheduler to schedule pods with matching taints. |
| volumeMounts | list | `[]` | Set additional volumeMounts on the output Deployment definition. |
| volumes | list | `[]` | Set additional volumes on the output Deployment definition. |

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
