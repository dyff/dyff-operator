// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

// +kubebuilder:validation:Optional
package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

// The item is retained if 'field `relation` value' is true.
// +kubebuilder:validation:Enum=`==`;`!=`;`<`;`<=`;`>`;`>=`
type FilterRelation string

type DatasetFilter struct {
	// +kubebuilder:validation:Required
	Field string `json:"field"`

	// +kubebuilder:validation:Required
	Relation FilterRelation `json:"relation"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:XIntOrString
	Value intstr.IntOrString `json:"value"`
}

type InferenceInterfaceSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Endpoint string `json:"endpoint"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	OutputSchema DataSchema `json:"outputSchema"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	InputPipeline []SchemaAdapter `json:"inputPipeline,omitempty"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	OutputPipeline []SchemaAdapter `json:"outputPipeline,omitempty"`
}

type EvaluationClientConfiguration struct {
	// +kubebuilder:default=Abort
	// +kubebuilder:validation:Enum=Abort;Skip
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	BadRequestPolicy string `json:"badRequestPolicy"`

	// +kubebuilder:default=120
	// +kubebuilder:validation:Minimum=0
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	TransientErrorRetryLimit int `json:"transientErrorRetryLimit"`

	// +kubebuilder:default=30
	// +kubebuilder:validation:Minimum=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	TransientErrorRetryDelaySeconds int `json:"transientErrorRetryDelaySeconds"`

	// +kubebuilder:default=Deduplicate
	// +kubebuilder:validation:Enum=Deduplicate;Error;Ignore
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	DuplicateOutputPolicy string `json:"duplicateOutputPolicy"`

	// +kubebuilder:default=Error
	// +kubebuilder:validation:Enum=Error;Ignore
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	MissingOutputPolicy string `json:"missingOutputPolicy"`

	// +kubebuilder:default=30
	// +kubebuilder:validation:Minimum=0
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	RequestTimeoutSeconds int `json:"requestTimeoutSeconds"`
}

// EvaluationSpec defines the desired state of Evaluation
type EvaluationSpec struct {
	DyffEntity `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Dataset EntityID `json:"dataset"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	InferenceSession *InferenceSessionTemplate `json:"inferenceSession,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	InferenceSessionReference *EntityID `json:"inferenceSessionReference,omitempty"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Interface InferenceInterfaceSpec `json:"interface"`

	// +kubebuilder:default=1
	// +kubebuilder:validation:Minimum=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Replications int32 `json:"replications"`

	// +kubebuilder:default=2
	// +kubebuilder:validation:Minimum=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	WorkersPerReplica int32 `json:"workersPerReplica"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Client EvaluationClientConfiguration `json:"client,omitempty"`
}

// EvaluationStatus defines the observed state of Evaluation
type EvaluationStatus struct {
	// +operator-sdk:csv:customresourcedefinitions:type=status
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Evaluation is the Schema for the evaluations API
type Evaluation struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   EvaluationSpec   `json:"spec,omitempty"`
	Status EvaluationStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// EvaluationList contains a list of Evaluation
type EvaluationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Evaluation `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Evaluation{}, &EvaluationList{})
}
