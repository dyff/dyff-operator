---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.11.1
  creationTimestamp: null
  name: inferencesessions.dyff.io
spec:
  group: dyff.io
  names:
    kind: InferenceSession
    listKind: InferenceSessionList
    plural: inferencesessions
    singular: inferencesession
  scope: Namespaced
  versions:
    - name: v1alpha1
      schema:
        openAPIV3Schema:
          description: InferenceSession is the Schema for the inferencesessions API
          properties:
            apiVersion:
              description:
                "APIVersion defines the versioned schema of this representation
                of an object. Servers should convert recognized schemas to the latest
                internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources"
              type: string
            kind:
              description:
                "Kind is a string value representing the REST resource this
                object represents. Servers may infer this from the endpoint the client
                submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds"
              type: string
            metadata:
              type: object
            spec:
              description: InferenceSessionSpec defines the desired state of InferenceSession
              properties:
                accelerator:
                  description:
                    Accelerator hardware to use *for each node*. For example,
                    if you specify 2 GPUs in the accelerator spec, and set .nodes =
                    2, then the session will require a total of 4 GPUs.
                  properties:
                    gpu:
                      properties:
                        count:
                          default: 1
                          format: int32
                          minimum: 1
                          type: integer
                        hardwareTypes:
                          items:
                            type: string
                          minItems: 1
                          type: array
                        memory:
                          anyOf:
                            - type: integer
                            - type: string
                          description:
                            "[DEPRECATED] Use .count if more than one GPU
                            is required"
                          pattern: ^(\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))(([KMGTPE]i)|[numkMGTPE]|([eE](\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))))?$
                          x-kubernetes-int-or-string: true
                      required:
                        - hardwareTypes
                      type: object
                    kind:
                      type: string
                  required:
                    - kind
                  type: object
                account:
                  type: string
                args:
                  description:
                    'Arguments to the entrypoint. The container image''s
                    CMD is used if this is not provided. Variable references $(VAR_NAME)
                    are expanded using the container''s environment. If a variable cannot
                    be resolved, the reference in the input string will be unchanged.
                    Double $$ are reduced to a single $, which allows for escaping the
                    $(VAR_NAME) syntax: i.e. "$$(VAR_NAME)" will produce the string
                    literal "$(VAR_NAME)". Escaped references will never be expanded,
                    regardless of whether the variable exists or not. Cannot be updated.
                    More info: https://kubernetes.io/docs/tasks/inject-data-application/define-command-argument-container/#running-a-command-in-a-shell'
                  items:
                    type: string
                  type: array
                artifactsVolume:
                  properties:
                    artifacts:
                      items:
                        properties:
                          id:
                            description:
                              The UUID4 identifier of the entity represented
                              in hex with no dashes. The null ID 0{32} is also allowed.
                              This is used for dependent resources.
                            pattern: (^[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}$)|(^0{32}$)
                            type: string
                          kind:
                            type: string
                        required:
                          - id
                          - kind
                        type: object
                      minItems: 1
                      type: array
                    mountPath:
                      type: string
                    name:
                      type: string
                    storage:
                      anyOf:
                        - type: integer
                        - type: string
                      pattern: ^(\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))(([KMGTPE]i)|[numkMGTPE]|([eE](\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))))?$
                      x-kubernetes-int-or-string: true
                  required:
                    - artifacts
                    - mountPath
                    - name
                    - storage
                  type: object
                command:
                  description:
                    'Entrypoint array. Not executed within a shell. The container
                    image''s ENTRYPOINT is used if this is not provided. Variable references
                    $(VAR_NAME) are expanded using the container''s environment. If
                    a variable cannot be resolved, the reference in the input string
                    will be unchanged. Double $$ are reduced to a single $, which allows
                    for escaping the $(VAR_NAME) syntax: i.e. "$$(VAR_NAME)" will produce
                    the string literal "$(VAR_NAME)". Escaped references will never
                    be expanded, regardless of whether the variable exists or not. Cannot
                    be updated. More info: https://kubernetes.io/docs/tasks/inject-data-application/define-command-argument-container/#running-a-command-in-a-shell'
                  items:
                    type: string
                  type: array
                dependencies:
                  items:
                    properties:
                      ephemeralVolume:
                        properties:
                          mountPath:
                            type: string
                          name:
                            type: string
                          storage:
                            anyOf:
                              - type: integer
                              - type: string
                            pattern: ^(\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))(([KMGTPE]i)|[numkMGTPE]|([eE](\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))))?$
                            x-kubernetes-int-or-string: true
                        required:
                          - mountPath
                          - name
                          - storage
                        type: object
                      fuseVolume:
                        properties:
                          artifact:
                            properties:
                              id:
                                description:
                                  The UUID4 identifier of the entity represented
                                  in hex with no dashes. The null ID 0{32} is also allowed.
                                  This is used for dependent resources.
                                pattern: (^[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}$)|(^0{32}$)
                                type: string
                              kind:
                                type: string
                            required:
                              - id
                              - kind
                            type: object
                          mountPath:
                            type: string
                          name:
                            type: string
                        required:
                          - artifact
                          - mountPath
                          - name
                        type: object
                      kind:
                        type: string
                      readOnlyVolume:
                        properties:
                          claimName:
                            type: string
                          mountPath:
                            type: string
                          name:
                            type: string
                        required:
                          - claimName
                          - mountPath
                          - name
                        type: object
                    required:
                      - kind
                    type: object
                  type: array
                env:
                  description:
                    List of environment variables to set in the container.
                    Cannot be updated.
                  items:
                    description:
                      EnvVar represents an environment variable present in
                      a Container.
                    properties:
                      name:
                        description: Name of the environment variable. Must be a C_IDENTIFIER.
                        type: string
                      value:
                        description:
                          'Variable references $(VAR_NAME) are expanded using
                          the previously defined environment variables in the container
                          and any service environment variables. If a variable cannot
                          be resolved, the reference in the input string will be unchanged.
                          Double $$ are reduced to a single $, which allows for escaping
                          the $(VAR_NAME) syntax: i.e. "$$(VAR_NAME)" will produce the
                          string literal "$(VAR_NAME)". Escaped references will never
                          be expanded, regardless of whether the variable exists or
                          not. Defaults to "".'
                        type: string
                      valueFrom:
                        description:
                          Source for the environment variable's value. Cannot
                          be used if value is not empty.
                        properties:
                          configMapKeyRef:
                            description: Selects a key of a ConfigMap.
                            properties:
                              key:
                                description: The key to select.
                                type: string
                              name:
                                description:
                                  "Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
                                  TODO: Add other useful fields. apiVersion, kind, uid?"
                                type: string
                              optional:
                                description:
                                  Specify whether the ConfigMap or its key
                                  must be defined
                                type: boolean
                            required:
                              - key
                            type: object
                            x-kubernetes-map-type: atomic
                          fieldRef:
                            description:
                              "Selects a field of the pod: supports metadata.name,
                              metadata.namespace, `metadata.labels['<KEY>']`, `metadata.annotations['<KEY>']`,
                              spec.nodeName, spec.serviceAccountName, status.hostIP,
                              status.podIP, status.podIPs."
                            properties:
                              apiVersion:
                                description:
                                  Version of the schema the FieldPath is
                                  written in terms of, defaults to "v1".
                                type: string
                              fieldPath:
                                description:
                                  Path of the field to select in the specified
                                  API version.
                                type: string
                            required:
                              - fieldPath
                            type: object
                            x-kubernetes-map-type: atomic
                          resourceFieldRef:
                            description:
                              "Selects a resource of the container: only
                              resources limits and requests (limits.cpu, limits.memory,
                              limits.ephemeral-storage, requests.cpu, requests.memory
                              and requests.ephemeral-storage) are currently supported."
                            properties:
                              containerName:
                                description:
                                  "Container name: required for volumes,
                                  optional for env vars"
                                type: string
                              divisor:
                                anyOf:
                                  - type: integer
                                  - type: string
                                description:
                                  Specifies the output format of the exposed
                                  resources, defaults to "1"
                                pattern: ^(\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))(([KMGTPE]i)|[numkMGTPE]|([eE](\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))))?$
                                x-kubernetes-int-or-string: true
                              resource:
                                description: "Required: resource to select"
                                type: string
                            required:
                              - resource
                            type: object
                            x-kubernetes-map-type: atomic
                          secretKeyRef:
                            description: Selects a key of a secret in the pod's namespace
                            properties:
                              key:
                                description:
                                  The key of the secret to select from.  Must
                                  be a valid secret key.
                                type: string
                              name:
                                description:
                                  "Name of the referent. More info: https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#names
                                  TODO: Add other useful fields. apiVersion, kind, uid?"
                                type: string
                              optional:
                                description:
                                  Specify whether the Secret or its key must
                                  be defined
                                type: boolean
                            required:
                              - key
                            type: object
                            x-kubernetes-map-type: atomic
                        type: object
                    required:
                      - name
                    type: object
                  type: array
                id:
                  description:
                    The UUID4 identifier of the entity represented in hex
                    with no dashes. The null ID 0{32} is also allowed. This is used
                    for dependent resources.
                  pattern: (^[0-9a-f]{12}4[0-9a-f]{3}[89ab][0-9a-f]{15}$)|(^0{32}$)
                  type: string
                image:
                  description:
                    "Container image name. More info: https://kubernetes.io/docs/concepts/containers/images
                    This field is optional to allow higher level config management to
                    default or override container images in workload controllers like
                    Deployments and StatefulSets."
                  type: string
                nodes:
                  default: 1
                  description: How many nodes to use for each replica.
                  format: int32
                  minimum: 1
                  type: integer
                replicas:
                  default: 1
                  format: int32
                  minimum: 1
                  type: integer
                resources:
                  description:
                    "Compute Resources required by this container. Cannot
                    be updated. More info: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/"
                  properties:
                    claims:
                      description:
                        "Claims lists the names of resources, defined in
                        spec.resourceClaims, that are used by this container. \n This
                        is an alpha field and requires enabling the DynamicResourceAllocation
                        feature gate. \n This field is immutable. It can only be set
                        for containers."
                      items:
                        description: ResourceClaim references one entry in PodSpec.ResourceClaims.
                        properties:
                          name:
                            description:
                              Name must match the name of one entry in pod.spec.resourceClaims
                              of the Pod where this field is used. It makes that resource
                              available inside a container.
                            type: string
                        required:
                          - name
                        type: object
                      type: array
                      x-kubernetes-list-map-keys:
                        - name
                      x-kubernetes-list-type: map
                    limits:
                      additionalProperties:
                        anyOf:
                          - type: integer
                          - type: string
                        pattern: ^(\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))(([KMGTPE]i)|[numkMGTPE]|([eE](\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))))?$
                        x-kubernetes-int-or-string: true
                      description:
                        "Limits describes the maximum amount of compute resources
                        allowed. More info: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/"
                      type: object
                    requests:
                      additionalProperties:
                        anyOf:
                          - type: integer
                          - type: string
                        pattern: ^(\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))(([KMGTPE]i)|[numkMGTPE]|([eE](\+|-)?(([0-9]+(\.[0-9]*)?)|(\.[0-9]+))))?$
                        x-kubernetes-int-or-string: true
                      description:
                        "Requests describes the minimum amount of compute
                        resources required. If Requests is omitted for a container,
                        it defaults to Limits if that is explicitly specified, otherwise
                        to an implementation-defined value. Requests cannot exceed Limits.
                        More info: https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/"
                      type: object
                  type: object
                useSpotPods:
                  default: true
                  type: boolean
              required:
                - account
                - id
              type: object
            status:
              description: InferenceSessionStatus defines the observed state of InferenceSession
              properties:
                conditions:
                  items:
                    description:
                      "Condition contains details for one aspect of the current
                      state of this API Resource. --- This struct is intended for direct
                      use as an array at the field path .status.conditions.  For example,
                      \n type FooStatus struct{ // Represents the observations of a
                      foo's current state. // Known .status.conditions.type are: \"Available\",
                      \"Progressing\", and \"Degraded\" // +patchMergeKey=type // +patchStrategy=merge
                      // +listType=map // +listMapKey=type Conditions []metav1.Condition
                      `json:\"conditions,omitempty\" patchStrategy:\"merge\" patchMergeKey:\"type\"
                      protobuf:\"bytes,1,rep,name=conditions\"` \n // other fields }"
                    properties:
                      lastTransitionTime:
                        description:
                          lastTransitionTime is the last time the condition
                          transitioned from one status to another. This should be when
                          the underlying condition changed.  If that is not known, then
                          using the time when the API field changed is acceptable.
                        format: date-time
                        type: string
                      message:
                        description:
                          message is a human readable message indicating
                          details about the transition. This may be an empty string.
                        maxLength: 32768
                        type: string
                      observedGeneration:
                        description:
                          observedGeneration represents the .metadata.generation
                          that the condition was set based upon. For instance, if .metadata.generation
                          is currently 12, but the .status.conditions[x].observedGeneration
                          is 9, the condition is out of date with respect to the current
                          state of the instance.
                        format: int64
                        minimum: 0
                        type: integer
                      reason:
                        description:
                          reason contains a programmatic identifier indicating
                          the reason for the condition's last transition. Producers
                          of specific condition types may define expected values and
                          meanings for this field, and whether the values are considered
                          a guaranteed API. The value should be a CamelCase string.
                          This field may not be empty.
                        maxLength: 1024
                        minLength: 1
                        pattern: ^[A-Za-z]([A-Za-z0-9_,:]*[A-Za-z0-9_])?$
                        type: string
                      status:
                        description: status of the condition, one of True, False, Unknown.
                        enum:
                          - "True"
                          - "False"
                          - Unknown
                        type: string
                      type:
                        description:
                          type of condition in CamelCase or in foo.example.com/CamelCase.
                          --- Many .condition.type values are consistent across resources
                          like Available, but because arbitrary conditions can be useful
                          (see .node.status.conditions), the ability to deconflict is
                          important. The regex it matches is (dns1123SubdomainFmt/)?(qualifiedNameFmt)
                        maxLength: 316
                        pattern: ^([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*/)?(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])$
                        type: string
                    required:
                      - lastTransitionTime
                      - message
                      - reason
                      - status
                      - type
                    type: object
                  type: array
              type: object
          type: object
      served: true
      storage: true
      subresources:
        status: {}
