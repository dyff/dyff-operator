// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package controller

import (
	"fmt"
	"os"
	"strings"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/cli-runtime/pkg/printers"
)

// Note: This constant is introduced in the kubebuilder tutorial with no
// explanation of how its value is chosen or what its semantics are. Based on
// the discussions referenced below, I think the value doesn't matter, or at
// least never makes it into the actual k8s resource, but I have no idea
// whether there are any rules for acceptable values.
// See: https://github.com/kubernetes-sigs/kubebuilder/pull/1424
// See: https://github.com/kubernetes-sigs/kubebuilder/issues/1422
const ownerKeyPrefix string = ".metadata.controller"

// Returns a pointer to the argument. Useful for when you want to provide a
// literal value but a pointer is expected.
func Pointer[T any](v T) *T {
	return &v
}

// Returns (true, JobConditionType) if the given Job has reached a terminal
// state. Otherwise, returns (false, _).
func isJobFinished(job *batchv1.Job) (finished bool, result batchv1.JobConditionType) {
	for _, condition := range job.Status.Conditions {
		finished := condition.Type == batchv1.JobComplete || condition.Type == batchv1.JobFailed
		if finished && condition.Status == corev1.ConditionTrue {
			return true, condition.Type
		}
	}
	return false, ""
}

func jobNames(jobs []*batchv1.Job) []string {
	var names []string
	for _, job := range jobs {
		names = append(names, job.Name)
	}
	return names
}

func jobsToYAML(jobs []*batchv1.Job) string {
	var builder strings.Builder
	yml := printers.YAMLPrinter{}

	for _, job := range jobs {
		yml.PrintObj(job, &builder)
	}
	return builder.String()
}

// FIXME: This is a temporary measure for schema compatibility
func lastIdComponent(id string) string {
	fields := strings.FieldsFunc(id, func(c rune) bool { return c == '/' })
	return fields[len(fields)-1]
}

// Returns the platform-specific init containers that should be used
func initContainers() []corev1.Container {
	var containers []corev1.Container
	if os.Getenv("DYFF_WORKFLOWS__GCLOUD__INIT_WORKLOAD_IDENTITY__IMAGE") != "" {
		containers = append(containers, corev1.Container{
			Name:  "init-workload-identity",
			Image: os.Getenv("DYFF_WORKFLOWS__GCLOUD__INIT_WORKLOAD_IDENTITY__IMAGE"),
			// GKE Autopilot minimums
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					"cpu":    resource.MustParse("250m"),
					"memory": resource.MustParse("512Mi"),
				},
				Limits: corev1.ResourceList{
					"cpu":    resource.MustParse("250m"),
					"memory": resource.MustParse("512Mi"),
				},
			},
		})
	}
	return containers
}

const ORDINARY_GROUP int64 = 1001
const ORDINARY_USER int64 = 1001

func workflowContainerSecurityContext() *corev1.SecurityContext {
	// containerSecurityContext:
	// 	capabilities:
	// 		# -- Set which capabilities to drop.
	// 		drop:
	// 		- ALL
	// 	# -- Mounts the container's root filesystem as read-only.
	// 	readOnlyRootFilesystem: true
	// 	# -- Set the primary group ID for all processes to run as within containers of the pods.
	// 	runAsGroup: 1001
	// 	# -- Set to run as root or non-root.
	// 	runAsNonRoot: true
	// 	# -- Set the primary user for all processes to run as within containers of the pods.
	// 	runAsUser: 1001

	var ordinary_group int64 = ORDINARY_GROUP
	var ordinary_user int64 = ORDINARY_USER
	return &corev1.SecurityContext{
		AllowPrivilegeEscalation: Pointer(false),
		Capabilities: &corev1.Capabilities{
			Drop: []corev1.Capability{"ALL"},
		},
		ReadOnlyRootFilesystem: Pointer(true),
		RunAsGroup:             &ordinary_group,
		RunAsNonRoot:           Pointer(true),
		RunAsUser:              &ordinary_user,
		SeccompProfile: &corev1.SeccompProfile{
			Type: corev1.SeccompProfileTypeRuntimeDefault,
		},
	}
}

func workflowsPodSecurityContext() *corev1.PodSecurityContext {
	var ordinary_group int64 = ORDINARY_GROUP
	return &corev1.PodSecurityContext{
		FSGroup: &ordinary_group,
	}
}

const (
	StatusComplete string = "Complete"
	StatusError    string = "Error"
	StatusFailed   string = "Failed"
	StatusReady    string = "Ready"
)

func artifactStorageUrl(kind string, id dyffv1alpha1.EntityID) string {
	if kind == "Dataset" {
		return fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__DATASETS__STORAGE__URL"), id)
	} else if kind == "Evaluation" {
		return fmt.Sprintf("%v/%v/verified", os.Getenv("DYFF_RESOURCES__OUTPUTS__STORAGE__URL"), id)
	} else if kind == "Measurement" {
		return fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__MEASUREMENTS__STORAGE__URL"), id)
	} else if kind == "Model" {
		return fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__MODELS__STORAGE__URL"), id)
	} else if kind == "Module" {
		return fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__MODULES__STORAGE__URL"), id)
	} else if kind == "Report" {
		return fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__REPORTS__STORAGE__URL"), id)
	} else if kind == "SafetyCase" {
		return fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__SAFETYCASES__STORAGE__URL"), id)
	} else {
		// kind is checked by k8s validation, so this should never happen
		panic(fmt.Sprintf("invalid kind %v", kind))
	}
}

type DyffReconcilerKernel struct {
	Workflow             string
	Kind                 string
	ShortName            string
	WorkflowSingleLetter string
	ServiceAccountName   string
}

func (k *DyffReconcilerKernel) ArtifactsDownload() kinds.WorkflowComponent {
	return kinds.WorkflowComponent{
		Kind:                  k.Kind,
		ShortName:             k.ShortName,
		Component:             "download",
		ShortComponent:        "down",
		SingleLetterComponent: "d",
	}
}

func (k *DyffReconcilerKernel) ArtifactsUpload() kinds.WorkflowComponent {
	return kinds.WorkflowComponent{
		Kind:                  k.Kind,
		ShortName:             k.ShortName,
		Component:             "upload",
		ShortComponent:        "up",
		SingleLetterComponent: "u",
	}
}

func (k *DyffReconcilerKernel) ArtifactsPVC() kinds.WorkflowComponent {
	return kinds.WorkflowComponent{
		Kind:                  k.Kind,
		ShortName:             k.ShortName,
		Component:             "artifacts",
		ShortComponent:        "artifacts",
		SingleLetterComponent: "a",
	}
}

func (k *DyffReconcilerKernel) downloadLabels(entitySpec *dyffv1alpha1.DyffEntity) map[string]string {
	return map[string]string{
		"dyff.io/account":   entitySpec.Account,
		"dyff.io/workflow":  k.Workflow,
		"dyff.io/step":      k.ArtifactsDownload().Component,
		"dyff.io/component": k.ArtifactsDownload().Component,
		"dyff.io/entity":    lastIdComponent(string(entitySpec.Id)),
	}
}

func (k *DyffReconcilerKernel) uploadLabels(entitySpec *dyffv1alpha1.DyffEntity) map[string]string {
	return map[string]string{
		"dyff.io/account":   entitySpec.Account,
		"dyff.io/workflow":  k.Workflow,
		"dyff.io/step":      k.ArtifactsUpload().Component,
		"dyff.io/component": k.ArtifactsUpload().Component,
		"dyff.io/entity":    lastIdComponent(string(entitySpec.Id)),
	}
}

func (k *DyffReconcilerKernel) artifactPersistentVolumeClaimLabels(entitySpec *dyffv1alpha1.DyffEntity) map[string]string {
	return map[string]string{
		"dyff.io/account":   entitySpec.Account,
		"dyff.io/workflow":  k.Workflow,
		"dyff.io/component": k.ArtifactsPVC().Component,
		"dyff.io/entity":    lastIdComponent(string(entitySpec.Id)),
	}
}

func (k *DyffReconcilerKernel) componentName(entitySpec *dyffv1alpha1.DyffEntity, component kinds.WorkflowComponent) string {
	id := lastIdComponent(string(entitySpec.Id))
	return fmt.Sprintf("%v%v%v", k.WorkflowSingleLetter, id, component.SingleLetterComponent)
}

func (k *DyffReconcilerKernel) artifactsVolumeMountPath() string {
	return "/dyff/mnt"
}

func (r *DyffReconcilerKernel) artifactsLocalPath(id dyffv1alpha1.EntityID) string {
	return fmt.Sprintf("%v/%v", r.artifactsVolumeMountPath(), id)
}

func (k *DyffReconcilerKernel) getArtifactsPersistentVolumeClaimName(entitySpec *dyffv1alpha1.DyffEntity) string {
	return k.componentName(entitySpec, k.ArtifactsPVC())
}

func (k *DyffReconcilerKernel) getArtifactsPersistentVolumeClaimManifestForEntity(
	entityMeta *metav1.ObjectMeta,
	entitySpec *dyffv1alpha1.DyffEntity,
	storage resource.Quantity,
) *corev1.PersistentVolumeClaim {
	return k.getArtifactsPersistentVolumeClaimManifest(
		k.artifactPersistentVolumeClaimLabels(entitySpec),
		k.getArtifactsPersistentVolumeClaimName(entitySpec),
		entityMeta.Namespace,
		storage,
	)
}

func (k *DyffReconcilerKernel) getArtifactsPersistentVolumeClaimManifest(
	labels map[string]string,
	name string,
	namespace string,
	storage resource.Quantity,
) *corev1.PersistentVolumeClaim {
	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    labels,
			Name:      name,
			Namespace: namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					"storage": storage,
				},
			},
		},
	}
}

func (k *DyffReconcilerKernel) getArtifactsDownloadJobManifestForEntity(
	artifacts []dyffv1alpha1.NamespacedID,
	entityMeta *metav1.ObjectMeta,
	entitySpec *dyffv1alpha1.DyffEntity,
) *batchv1.Job {
	return k.getArtifactsDownloadJobManifest(
		artifacts,
		k.downloadLabels(entitySpec),
		k.componentName(entitySpec, kinds.AnalysisDownload()),
		entityMeta.Namespace,
		k.getArtifactsPersistentVolumeClaimName(entitySpec),
	)
}

func (k *DyffReconcilerKernel) getArtifactsDownloadJobManifest(
	artifacts []dyffv1alpha1.NamespacedID,
	labels map[string]string,
	name string,
	namespace string,
	pvcName string,
) *batchv1.Job {
	var transfers []string

	for _, artifact := range artifacts {
		storagePath := artifactStorageUrl(artifact.Kind, artifact.Id)
		localPath := k.artifactsLocalPath(artifact.Id)
		transfers = append(transfers, fmt.Sprintf("%v>%v", storagePath, localPath))
	}

	// Command-line arguments
	args := make([]string, len(transfers))
	for i := range transfers {
		args[i] = fmt.Sprintf("--transfer=%v", transfers[i])
	}

	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    labels,
			Name:      name,
			Namespace: namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  name,
							Image: os.Getenv("DYFF_WORKFLOWS__COMMON__STORAGE_TRANSFER__IMAGE"),
							Args:  args,
							EnvFrom: []corev1.EnvFromSource{
								{
									ConfigMapRef: &corev1.ConfigMapEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-config",
										},
									},
								},
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-storage-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("2Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("2Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: k.artifactsVolumeMountPath(),
								},
							},
						},
					},
					RestartPolicy:      "Never",
					SecurityContext:    workflowsPodSecurityContext(),
					ServiceAccountName: k.ServiceAccountName,
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
					},
				},
			},
		},
	}
}

func (k *DyffReconcilerKernel) getArtifactsUploadJobManifest(
	artifactKind string,
	entityMeta *metav1.ObjectMeta,
	entitySpec *dyffv1alpha1.DyffEntity,
) *batchv1.Job {
	pvcName := k.componentName(entitySpec, k.ArtifactsPVC())

	localPath := k.artifactsLocalPath(entitySpec.Id)
	storagePath := artifactStorageUrl(artifactKind, entitySpec.Id)
	transfer := fmt.Sprintf("%v>%v", localPath, storagePath)

	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    k.uploadLabels(entitySpec),
			Name:      k.componentName(entitySpec, k.ArtifactsUpload()),
			Namespace: entityMeta.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: k.uploadLabels(entitySpec),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  k.componentName(entitySpec, k.ArtifactsUpload()),
							Image: os.Getenv("DYFF_WORKFLOWS__COMMON__STORAGE_TRANSFER__IMAGE"),
							// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/storage-transfer:latest",
							Args: []string{
								fmt.Sprintf("--transfer=%v", transfer),
							},
							EnvFrom: []corev1.EnvFromSource{
								{
									ConfigMapRef: &corev1.ConfigMapEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-config",
										},
									},
								},
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-storage-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: k.artifactsVolumeMountPath(),
								},
							},
						},
					},
					RestartPolicy:      "Never",
					SecurityContext:    workflowsPodSecurityContext(),
					ServiceAccountName: k.ServiceAccountName,
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
					},
				},
			},
		},
	}
}

type DyffReconcilerInterface interface {
}

func isStatusConditionSuccess(conditions []metav1.Condition) bool {
	return k8sMeta.IsStatusConditionTrue(conditions, StatusComplete) || k8sMeta.IsStatusConditionTrue(conditions, StatusReady)
}
