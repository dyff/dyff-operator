// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package tokens

import (
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
	dyff "gitlab.com/dyff/dyff-operator/api/v1alpha1"
)

type APIFunction string

const (
	Create   APIFunction = "create"
	Get      APIFunction = "get"
	Query    APIFunction = "query"
	Download APIFunction = "download"
	Upload   APIFunction = "upload"
	Data     APIFunction = "data"
	Strata   APIFunction = "strata"
	All      APIFunction = "*"
)

type AccessGrant struct {
	Resource  string
	Account   dyff.EntityID
	Entity    dyff.EntityID
	Functions []APIFunction
}

func (grant AccessGrant) toClaims() jwt.MapClaims {
	var functions []string
	for _, function := range grant.Functions {
		functions = append(functions, string(function))
	}
	return jwt.MapClaims{
		"resource":  grant.Resource,
		"account":   grant.Account,
		"entity":    grant.Entity,
		"functions": functions,
	}
}

type APIKey struct {
	Subject string
	Created jwt.NumericDate
	Expires jwt.NumericDate
	Secret  string
	Grants  []AccessGrant
}

func (apiKey APIKey) toClaims() jwt.MapClaims {
	var grants []jwt.MapClaims
	for _, grant := range apiKey.Grants {
		grants = append(grants, grant.toClaims())
	}
	return jwt.MapClaims{
		"sub":    apiKey.Subject,
		"nbf":    apiKey.Created,
		"exp":    apiKey.Expires,
		"iat":    jwt.NewNumericDate(time.Now()),
		"grants": grants,
	}
}

func SignAPIKey(secret []byte, apiKey APIKey) (string, error) {
	var algorithm string = "HS256"
	var signingMethod = jwt.GetSigningMethod(algorithm)
	var token *jwt.Token = jwt.NewWithClaims(signingMethod, apiKey.toClaims())
	token.Header["typ"] = "JWT"
	token.Header["alg"] = algorithm
	return token.SignedString(secret)
}
