// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type ModelSourceSpecGitLFS struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Url string `json:"url"`
}

type ModelSourceSpecHuggingFaceHub struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	RepoID string `json:"repoID"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Revision string `json:"revision"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	AllowPatterns []string `json:"allowPatterns,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	IgnorePatterns []string `json:"ignorePatterns,omitempty"`
}

type ModelSourceSpecOpenLLM struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ModelKind string `json:"modelKind"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ModelID string `json:"modelID"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ModelVersion string `json:"modelVersion"`
}

type ModelSourceSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	GitLFS *ModelSourceSpecGitLFS `json:"gitLFS,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	HuggingFaceHub *ModelSourceSpecHuggingFaceHub `json:"huggingFaceHub,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	OpenLLM *ModelSourceSpecOpenLLM `json:"openLLM,omitempty"`
}

type ModelStorageObjectStoreSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Procotol string `json:"protocol"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	BucketName string `json:"bucketName"`
}

type ModelStoragePersistentVolumeSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	StorageClassName string `json:"storageClassName"`
}

type ModelStorageSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Quantity resource.Quantity `json:"quantity"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ObjectStore *ModelStorageObjectStoreSpec `json:"objectStore"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	PersistentVolume *ModelStoragePersistentVolumeSpec `json:"persistentVolume"`
}

// ModelSpec defines the desired state of Model
type ModelSpec struct {
	DyffEntity `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Source ModelSourceSpec `json:"source"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Storage ModelStorageSpec `json:"storage"`
}

// ModelStatus defines the observed state of Model
type ModelStatus struct {
	// +operator-sdk:csv:customresourcedefinitions:type=status
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Model is the Schema for the models API
type Model struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ModelSpec   `json:"spec,omitempty"`
	Status ModelStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ModelList contains a list of Model
type ModelList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Model `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Model{}, &ModelList{})
}
