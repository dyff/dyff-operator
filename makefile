# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

LOCALBIN ?= $(shell pwd)/bin
IMAGE ?= dyff-operator
OPERATOR_BIN_NAME ?= dyff-operator
OPERATOR_BIN_PATH ?= $(LOCALBIN)/$(OPERATOR_BIN_NAME)

CONTROLLER_GEN ?= $(LOCALBIN)/controller-gen
CONTROLLER_TOOLS_VERSION ?= v0.11.1

ENVTEST ?= $(LOCALBIN)/setup-envtest
ENVTEST_K8S_VERSION = 1.26.0

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

.PHONY: all
all: build

.PHONY: manifests
manifests: controller-gen ## Generate WebhookConfiguration, ClusterRole and CustomResourceDefinition objects.
	$(CONTROLLER_GEN) rbac:roleName=manager-role crd webhook paths="./..." output:crd:artifacts:config=config/crd/bases

.PHONY: generate
generate: controller-gen ## Generate code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.
	$(CONTROLLER_GEN) object:headerFile="hack/boilerplate.go.txt" paths="./..."

HELM_CRD_DIR = chart/templates/crds
KUSTOMIZE_CRD_DIR = config/crd/bases

KUSTOMIZE_CRDS = $(shell find $(KUSTOMIZE_CRD_DIR)/ -name '*.yaml' -type f)
HELM_CRDS = $(patsubst $(KUSTOMIZE_CRD_DIR)/dyff.io_%.yaml,$(HELM_CRD_DIR)/crd-%.yaml,$(KUSTOMIZE_CRDS))

.PHONY: helm-crds $(KUSTOMIZE_CRDS)
helm-crds: $(HELM_CRDS)

$(HELM_CRD_DIR):
	mkdir -p $(HELM_CRD_DIR)

$(HELM_CRD_DIR)/crd-%.yaml: $(KUSTOMIZE_CRD_DIR)/dyff.io_%.yaml | $(HELM_CRD_DIR)
	@echo "$^ -> $@"
	@echo "{{- if .Values.crds.enabled }}" > "$@"
	@tail -n +2 "$^" >> "$@"
	@echo "{{- end }}" >> "$@"

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: vet
vet:
	go vet ./...

.PHONY: test
test: manifests generate fmt vet envtest
	KUBEBUILDER_ASSETS="$(shell $(ENVTEST) use $(ENVTEST_K8S_VERSION) --bin-dir $(LOCALBIN) -p path)" go test ./... -coverprofile cover.out

.PHONY: build
build: manifests generate fmt vet | $(LOCALBIN)
	go build -o $(OPERATOR_BIN_PATH) cmd/main.go

.PHONY: run
run: manifests generate fmt vet
	go run ./cmd/main.go

.PHONY: gcloud-build
gcloud-build: test
	gcloud builds submit --config gcloud-build.yaml .

.PHONY: docker-build
docker-build:
	docker build -t $(IMAGE) .

.PHONY: docker-run
docker-run:
	docker run --rm -it $(IMAGE) .

$(LOCALBIN):
	mkdir -p $(LOCALBIN)

.PHONY: controller-gen
controller-gen: $(CONTROLLER_GEN)

## Download controller-gen locally if necessary. If wrong version is installed, it will be overwritten.
$(CONTROLLER_GEN): $(LOCALBIN)
	test -s $(LOCALBIN)/controller-gen && $(LOCALBIN)/controller-gen --version | grep -q $(CONTROLLER_TOOLS_VERSION) || \
	GOBIN=$(LOCALBIN) go install sigs.k8s.io/controller-tools/cmd/controller-gen@$(CONTROLLER_TOOLS_VERSION)

.PHONY: envtest
envtest: $(ENVTEST)

## Download envtest-setup locally if necessary.
$(ENVTEST): $(LOCALBIN)
	test -s $(LOCALBIN)/setup-envtest || GOBIN=$(LOCALBIN) go install sigs.k8s.io/controller-runtime/tools/setup-envtest@latest

.PHONY: prettier
prettier:
	npm_config_yes=true npx prettier --version
	npx prettier -w .
