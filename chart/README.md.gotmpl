# dyff-operator

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/dyff-operator)](https://artifacthub.io/packages/search?repo=dyff-operator)

Operator to manage deployments of Dyff CRDs.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Installation

```bash
helm install dyff-operator oci://registry.gitlab.com/dyff/charts/dyff-operator
```

## Removal

Now the chart can be deleted:

```bash
helm uninstall dyff-operator
```

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
