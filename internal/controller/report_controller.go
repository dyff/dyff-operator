// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

// SECURITY: This workflow executes untrusted code
// The following measures must be employed for untrusted pods:
//   1. No (=> default) service account (#security.service_account)
//   2. label 'security.dyff.io/untrusted: "true"' is applied
//      => deny-all NetworkPolicy applies (#security.untrusted_label)
//      FIXME: Should use whitelist (security.dyff.io/trusted) instead
//   3. run in gVisor (#security.gvisor)
//   4. Set a timeout on the job (#security.timeout)

package controller

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
)

// ReportReconciler reconciles a Report object
type ReportReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	ownerKey string
}

func ReportWorkflowPlural() string {
	return "reports"
}

func ReportWorkflowSingleLetter() string {
	return "r"
}

func (r *ReportReconciler) StatusDownloadSuccessful() string {
	return "reports.dyff.io/DownloadSuccessful"
}
func (r *ReportReconciler) StatusRunSuccessful() string {
	return "reports.dyff.io/RunSuccessful"
}
func (r *ReportReconciler) StatusUploadSuccessful() string {
	return "reports.dyff.io/UploadSuccessful"
}

// Returns the name that should be assigned to a child k8s resource according
// to which component of the Report workflow the child resource belongs to.
func (r *ReportReconciler) componentName(report *dyffv1alpha1.Report, component kinds.WorkflowComponent) string {
	id := lastIdComponent(string(report.Spec.Id))
	return fmt.Sprintf("%v-%v", id, component.SingleLetterComponent)
}

func (r *ReportReconciler) pvcLabels(report *dyffv1alpha1.Report) map[string]string {
	return map[string]string{
		"dyff.io/account":   report.Spec.Account,
		"dyff.io/workflow":  ReportWorkflowPlural(),
		"dyff.io/component": "pvc",
		"dyff.io/report":    lastIdComponent(string(report.Spec.Id)),
	}
}

func (r *ReportReconciler) downloadLabels(report *dyffv1alpha1.Report) map[string]string {
	return map[string]string{
		"dyff.io/account":   report.Spec.Account,
		"dyff.io/workflow":  ReportWorkflowPlural(),
		"dyff.io/step":      kinds.ReportDownload().Component,
		"dyff.io/component": kinds.ReportDownload().Component,
		"dyff.io/report":    lastIdComponent(string(report.Spec.Id)),
	}
}

func (r *ReportReconciler) uploadLabels(report *dyffv1alpha1.Report) map[string]string {
	return map[string]string{
		"dyff.io/account":   report.Spec.Account,
		"dyff.io/workflow":  ReportWorkflowPlural(),
		"dyff.io/step":      kinds.ReportUpload().Component,
		"dyff.io/component": kinds.ReportUpload().Component,
		"dyff.io/report":    lastIdComponent(string(report.Spec.Id)),
	}
}

func (r *ReportReconciler) runLabels(report *dyffv1alpha1.Report) map[string]string {
	// SECURITY: This pod executes untrusted code
	// See comment at top of file
	return map[string]string{
		"dyff.io/account":   report.Spec.Account,
		"dyff.io/workflow":  ReportWorkflowPlural(),
		"dyff.io/step":      kinds.ReportRun().Component,
		"dyff.io/component": kinds.ReportRun().Component,
		"dyff.io/report":    lastIdComponent(string(report.Spec.Id)),
		// FIXME: Should use a whitelist rather than a blacklist
		// i.e., default is "untrusted", must mark things as "trusted"
		"security.dyff.io/untrusted": "true", // #security.untrusted_label
	}
}

func (r *ReportReconciler) createTemporaryPVC(report *dyffv1alpha1.Report) *corev1.PersistentVolumeClaim {
	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.pvcLabels(report),
			Name:      r.componentName(report, kinds.ReportPVC()),
			Namespace: report.Namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					"storage": resource.MustParse("10Gi"),
				},
			},
		},
	}
}

func (r *ReportReconciler) pvcMountPath() string {
	return "/dyff/mnt"
}

func (r *ReportReconciler) artifactLocalPath(id dyffv1alpha1.EntityID) string {
	return fmt.Sprintf("%v/%v", r.pvcMountPath(), id)
}

func (r *ReportReconciler) createDownloadJob(report *dyffv1alpha1.Report) *batchv1.Job {
	var transfers []string

	pvcName := r.componentName(report, kinds.ReportPVC())
	datasetPath := fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__DATASETS__STORAGE__URL"), report.Spec.Dataset)
	evaluationPath := fmt.Sprintf("%v/%v/verified", os.Getenv("DYFF_RESOURCES__OUTPUTS__STORAGE__URL"), report.Spec.Evaluation)
	transfers = append(transfers, fmt.Sprintf("%v>%v", datasetPath, r.artifactLocalPath(report.Spec.Dataset)))
	transfers = append(transfers, fmt.Sprintf("%v>%v", evaluationPath, r.artifactLocalPath(report.Spec.Evaluation)))

	// Extension modules needed by the Report
	for _, module := range report.Spec.Modules {
		modulePath := fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__MODULES__STORAGE__URL"), module)
		moduleTransfer := fmt.Sprintf("%v>%v", modulePath, r.artifactLocalPath(module))
		transfers = append(transfers, moduleTransfer)
	}

	// Command-line arguments
	args := make([]string, len(transfers))
	for i := range transfers {
		args[i] = fmt.Sprintf("--transfer=%v", transfers[i])
	}

	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.downloadLabels(report),
			Name:      r.componentName(report, kinds.ReportDownload()),
			Namespace: report.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.downloadLabels(report),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  r.componentName(report, kinds.ReportDownload()),
							Image: os.Getenv("DYFF_WORKFLOWS__REPORTS__DOWNLOAD__IMAGE"),
							// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/storage-transfer:latest",
							Args: args,
							EnvFrom: []corev1.EnvFromSource{
								{
									ConfigMapRef: &corev1.ConfigMapEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-config",
										},
									},
								},
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-storage-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: r.pvcMountPath(),
								},
							},
						},
					},
					SecurityContext: workflowsPodSecurityContext(),
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
					},
					RestartPolicy:      "Never",
					ServiceAccountName: "report-runner",
				},
			},
		},
	}
}

func (r *ReportReconciler) createUploadJob(report *dyffv1alpha1.Report) *batchv1.Job {
	pvcName := r.componentName(report, kinds.ReportPVC())
	reportPath := fmt.Sprintf("%v/%v", os.Getenv("DYFF_RESOURCES__REPORTS__STORAGE__URL"), report.Spec.Id)
	reportTransfer := fmt.Sprintf("%v>%v", r.artifactLocalPath(report.Spec.Id), reportPath)
	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.uploadLabels(report),
			Name:      r.componentName(report, kinds.ReportUpload()),
			Namespace: report.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.uploadLabels(report),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  r.componentName(report, kinds.ReportUpload()),
							Image: os.Getenv("DYFF_WORKFLOWS__REPORTS__UPLOAD__IMAGE"),
							// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/storage-transfer:latest",
							Args: []string{
								fmt.Sprintf("--transfer=%v", reportTransfer),
							},
							EnvFrom: []corev1.EnvFromSource{
								{
									ConfigMapRef: &corev1.ConfigMapEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-config",
										},
									},
								},
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "workflows-storage-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("4Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: r.pvcMountPath(),
								},
							},
						},
					},
					SecurityContext: workflowsPodSecurityContext(),
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
					},
					RestartPolicy:      "Never",
					ServiceAccountName: "report-runner",
				},
			},
		},
	}
}

func (r *ReportReconciler) createRunJob(report *dyffv1alpha1.Report) *batchv1.Job {
	// SECURITY: This pod executes untrusted code
	// See comment at top of file
	pvcName := r.componentName(report, kinds.ReportPVC())
	tmpName := r.componentName(report, kinds.ReportTmpVolume())
	const tmpPath string = "/tmp"

	args := []string{
		// FIXME: Rename 'Report' -> 'Rubric' in CRD
		fmt.Sprintf("--rubric=%v", report.Spec.Report),
		fmt.Sprintf("--dataset_path=%v", r.artifactLocalPath(report.Spec.Dataset)),
		fmt.Sprintf("--evaluation_path=%v", r.artifactLocalPath(report.Spec.Evaluation)),
		fmt.Sprintf("--output_path=%v", r.artifactLocalPath(report.Spec.Id)),
	}

	// Extension modules needed by the Report
	for _, module := range report.Spec.Modules {
		modulePath := r.artifactLocalPath(module)
		args = append(args, fmt.Sprintf("--module=%v", modulePath))
	}

	var backoffLimit int32 = 1

	var runtimeClassNamePointer *string
	// We might use the address of the value, so be careful about aliasing
	runtimeClassName, ok := os.LookupEnv("DYFF_WORKFLOWS__REPORTS__RUN__RUNTIME_CLASS_NAME")
	if ok {
		runtimeClassNamePointer = &runtimeClassName // #security.gvisor
	}

	var activeDeadlineSeconds int64 = 600 // #security.timeout
	activeDeadlineSecondsValue, ok := os.LookupEnv("DYFF_WORKFLOWS__REPORTS__RUN__ACTIVE_DEADLINE_SECONDS")
	if ok {
		seconds, err := strconv.ParseInt(activeDeadlineSecondsValue, 10, 64)
		if err == nil {
			activeDeadlineSeconds = seconds
		}
	}

	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.runLabels(report),
			Name:      r.componentName(report, kinds.ReportRun()),
			Namespace: report.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			// Do not specify a service account #security.service_account
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.runLabels(report),
				},
				Spec: corev1.PodSpec{
					RuntimeClassName:      runtimeClassNamePointer,
					ActiveDeadlineSeconds: &activeDeadlineSeconds,
					Containers: []corev1.Container{
						{
							Name:  r.componentName(report, kinds.ReportRun()),
							Image: os.Getenv("DYFF_WORKFLOWS__REPORTS__RUN__IMAGE"),
							Env: []corev1.EnvVar{
								{
									Name:  "DYFF_AUDIT_LOCAL_STORAGE_ROOT",
									Value: r.pvcMountPath(),
								},
								{
									Name:  "HOME",
									Value: tmpPath,
								},
							},
							Args: args,
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("8000m"),
									"memory": resource.MustParse("16Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("8000m"),
									"memory": resource.MustParse("16Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      pvcName,
									MountPath: r.pvcMountPath(),
								},
								{
									Name:      tmpName,
									MountPath: tmpPath,
								},
							},
						},
					},
					SecurityContext: workflowsPodSecurityContext(),
					Volumes: []corev1.Volume{
						{
							Name: pvcName,
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: pvcName,
								},
							},
						},
						{
							Name: tmpName,
							VolumeSource: corev1.VolumeSource{
								Ephemeral: &corev1.EphemeralVolumeSource{
									VolumeClaimTemplate: &corev1.PersistentVolumeClaimTemplate{
										Spec: corev1.PersistentVolumeClaimSpec{
											AccessModes: []corev1.PersistentVolumeAccessMode{"ReadWriteOnce"},
											Resources: corev1.ResourceRequirements{
												Requests: corev1.ResourceList{
													"storage": resource.MustParse("16Gi"),
												},
											},
											// TODO: GKE-specific name
											// StorageClassName: Pointer("standard-rwo"),
										},
									},
								},
							},
						},
					},
					RestartPolicy: "Never",
				},
			},
		},
	}
}

// If the workflow has reached a terminal Condition, return that Condition. The
// operator should clean up any resources and set the returned Condition in the
// workflow k8s manifest.
//
// Else, return nil. The operator should let the workflow continue running.
func (r *ReportReconciler) terminalCondition(conditions []metav1.Condition) *metav1.Condition {
	var rootCause *metav1.Condition
	var condition *metav1.Condition

	// If top-level terminal condition is already set, return it
	for _, conditionType := range []string{StatusComplete, StatusFailed} {
		condition = k8sMeta.FindStatusCondition(conditions, conditionType)
		if condition != nil && condition.Status == metav1.ConditionTrue {
			return condition
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, r.StatusDownloadSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	} else if condition.Status == metav1.ConditionFalse {
		// Fail immediately if Download failed
		return &metav1.Condition{
			Type:               StatusFailed,
			Status:             metav1.ConditionTrue,
			Reason:             "DownloadStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Download step Failed.",
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, r.StatusRunSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	} else if condition.Status == metav1.ConditionFalse {
		// If Run failed, we still want to Upload the log data [*]
		rootCause = &metav1.Condition{
			Type:               StatusFailed,
			Status:             metav1.ConditionTrue,
			Reason:             "RunStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Run step Failed.",
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, r.StatusUploadSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	}

	// [*] Now we can report Failed
	// Even if Upload failed, too, it's more important that the user know that
	// the Run step failed, and we can only report one status.
	if rootCause != nil {
		return rootCause
	}

	if condition.Status == metav1.ConditionFalse {
		// Fail immediately if Upload failed
		return &metav1.Condition{
			Type:               StatusFailed,
			Status:             metav1.ConditionTrue,
			Reason:             "UploadStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Upload step Failed.",
		}
	}

	// Everything succeeded
	return &metav1.Condition{
		Type:               StatusComplete,
		Status:             metav1.ConditionTrue,
		Reason:             StatusComplete,
		LastTransitionTime: metav1.NewTime(time.Now()),
		Message:            "All steps are Complete.",
	}
}

//+kubebuilder:rbac:groups=dyff.io,resources=reports,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=reports/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=reports/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Report object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *ReportReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var err error
	var logger = log.FromContext(ctx)

	// Get the Report
	var report dyffv1alpha1.Report
	err = r.Get(ctx, req.NamespacedName, &report)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("Report NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get Report")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// If in a terminal condition, set condition, clean up resources, return
	if terminal := r.terminalCondition(report.Status.Conditions); terminal != nil {
		logger.Info("terminal status: ", "status", terminal)

		k8sMeta.SetStatusCondition(&report.Status.Conditions, *terminal)
		err = r.Status().Update(ctx, &report)
		if err != nil {
			return ctrl.Result{}, err
		}

		// Only delete the Jobs after Condition updates are visible, since we
		// use Job status as a cue for which step of the workflow we're in.
		err = r.Delete(ctx, r.createDownloadJob(&report), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
		err = r.Delete(ctx, r.createRunJob(&report), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
		err = r.Delete(ctx, r.createUploadJob(&report), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		// Shared dependencies
		err = r.Delete(ctx, r.createTemporaryPVC(&report))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	// Get child Jobs
	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	var downloadFailed bool = false
	var runFailed bool = false
	var uploadFailed bool = false
	for _, job := range failedJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.ReportDownload().Component {
			downloadFailed = true
		} else if step == kinds.ReportRun().Component {
			runFailed = true
		} else if step == kinds.ReportUpload().Component {
			uploadFailed = true
		}
	}

	var downloadSucceeded bool = false
	var runSucceeded bool = false
	var uploadSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.ReportDownload().Component {
			downloadSucceeded = true
		} else if step == kinds.ReportRun().Component {
			runSucceeded = true
		} else if step == kinds.ReportUpload().Component {
			uploadSucceeded = true
		}
	}

	if uploadSucceeded || uploadFailed {
		if uploadSucceeded {
			k8sMeta.SetStatusCondition(
				&report.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusUploadSuccessful(),
					Status:             metav1.ConditionTrue,
					Reason:             "UploadStepComplete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Upload step is Complete.",
				},
			)
			err = r.Status().Update(ctx, &report)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("report workflow succeeded")
		} else if uploadFailed {
			k8sMeta.SetStatusCondition(
				&report.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusUploadSuccessful(),
					Status:             metav1.ConditionFalse,
					Reason:             "UploadStepFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Upload step Failed.",
				},
			)
			err = r.Status().Update(ctx, &report)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("report workflow failed")
		}
	} else if runSucceeded || runFailed {
		if runSucceeded {
			k8sMeta.SetStatusCondition(
				&report.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusRunSuccessful(),
					Status:             metav1.ConditionTrue,
					Reason:             "RunStepComplete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Run step is Complete.",
				},
			)
			err = r.Status().Update(ctx, &report)
			if err != nil {
				return ctrl.Result{}, err
			}
		} else if runFailed {
			k8sMeta.SetStatusCondition(
				&report.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusRunSuccessful(),
					Status:             metav1.ConditionFalse,
					Reason:             "RunStepFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Run step Failed.",
				},
			)
			err = r.Status().Update(ctx, &report)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("report workflow failed")
		}

		// Next step is Upload, even if Run failed, so that we can preserve logs
		uploadJob := r.createUploadJob(&report)
		if err := ctrl.SetControllerReference(&report, uploadJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt upload Job")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, uploadJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create upload Job", "uploadJob", uploadJob)
			return ctrl.Result{}, err
		}

		logger.Info("created workflow step: report.upload")
	} else if downloadSucceeded || downloadFailed {
		if downloadSucceeded {
			k8sMeta.SetStatusCondition(
				&report.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusDownloadSuccessful(),
					Status:             metav1.ConditionTrue,
					Reason:             "DownloadStepComplete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Download step is Complete.",
				},
			)
			err = r.Status().Update(ctx, &report)
			if err != nil {
				return ctrl.Result{}, err
			}

			// Next step is Run
			runJob := r.createRunJob(&report)
			if err := ctrl.SetControllerReference(&report, runJob, r.Scheme); err != nil {
				logger.Error(err, "failed to adopt run Job")
				return ctrl.Result{}, err
			}
			err = r.Create(ctx, runJob)
			if client.IgnoreAlreadyExists(err) != nil {
				logger.Error(err, "failed to create run Job", "runJob", runJob)
				return ctrl.Result{}, err
			}
			logger.Info("created workflow step: report.run")
		} else if downloadFailed {
			k8sMeta.SetStatusCondition(
				&report.Status.Conditions,
				metav1.Condition{
					Type:               r.StatusDownloadSuccessful(),
					Status:             metav1.ConditionFalse,
					Reason:             "DownloadStepFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "The Download step Failed.",
				},
			)
			err = r.Status().Update(ctx, &report)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("report workflow failed")
		}
	} else {
		// Nothing has run yet

		// Create shared dependencies
		pvc := r.createTemporaryPVC(&report)
		if err := ctrl.SetControllerReference(&report, pvc, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt shared PVC")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, pvc)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create shared PVC", "pvc", pvc)
			return ctrl.Result{}, err
		}

		// First step is Download
		downloadJob := r.createDownloadJob(&report)
		if err := ctrl.SetControllerReference(&report, downloadJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt download Job")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, downloadJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create download Job", "downloadJob", downloadJob)
			return ctrl.Result{}, err
		}

		logger.Info("created workflow step: report.download")

		k8sMeta.SetStatusCondition(
			&report.Status.Conditions,
			metav1.Condition{
				Type:               StatusComplete,
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Report is running.",
			},
		)
		k8sMeta.SetStatusCondition(
			&report.Status.Conditions,
			metav1.Condition{
				Type:               StatusFailed,
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Report is running.",
			},
		)
		if err := r.Status().Update(ctx, &report); err != nil {
			return ctrl.Result{}, err
		}
	}

	// Return OK
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ReportReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.Report)

	// Cache Jobs whose owner is an Report
	err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.Report {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": ReportWorkflowPlural(),
		},
	})
	if err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.Report{}).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.PersistentVolumeClaim{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
