// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package controller

import (
	"context"
	"encoding/base64"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/cli-runtime/pkg/printers"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
	"gitlab.com/dyff/dyff-operator/internal/tokens"
)

// FIXME: SECURITY This function currently grants access to *all resources* for
// debugging. We need to implement a mechanism where an Audit can specify
// what resources it requires, we check that it should actually have access
// to those resources, and then we issue a key for only those resources.
func generateAuditAPIKey(audit dyffv1alpha1.EntityID) tokens.APIKey {
	var now time.Time = time.Now()
	lifetime, err := time.ParseDuration("1h")
	if err != nil {
		panic(err)
	}
	return tokens.APIKey{
		Subject: fmt.Sprintf("Audit/%v", audit),
		Created: *jwt.NewNumericDate(now),
		Expires: *jwt.NewNumericDate(now.Add(lifetime)),
		Grants: []tokens.AccessGrant{{
			Resource: "*",
			Account:  "*",
			Entity:   "*",
			Functions: []tokens.APIFunction{
				tokens.Get, tokens.Query, tokens.Data, tokens.Strata, tokens.Download, tokens.Upload,
			},
		}},
	}
}

// AuditReconciler reconciles a Audit object
type AuditReconciler struct {
	client.Client
	Scheme              *runtime.Scheme
	ownerKey            string
	apiKeySigningSecret []byte
}

func (r *AuditReconciler) getSigningSecret(ctx context.Context) []byte {
	if r.apiKeySigningSecret == nil {
		const secretVarName string = "ALIGNMENTLABS_API_KEY_SIGNING_SECRET"
		var logger = log.FromContext(ctx)
		var signingKeyData string = os.Getenv(secretVarName)
		// We allow this for convenient local debugging, but in the cluster
		// we should provide an env variable to avoid lots of k8s API calls.
		if signingKeyData == "" {
			logger.Info("getting signing secret via k8s API")
			var k8sSecret corev1.Secret
			_ = r.Get(ctx, types.NamespacedName{Namespace: "default", Name: "api-key-signing"}, &k8sSecret)
			signingKeyData = string(k8sSecret.Data[secretVarName])
		}

		signingKeyStringDataBuf := make([]byte, base64.URLEncoding.DecodedLen(len(signingKeyData)))
		n, err := base64.URLEncoding.Decode(signingKeyStringDataBuf, []byte(signingKeyData))
		if err != nil {
			return nil
		}
		var signingKeyStringData []byte = signingKeyStringDataBuf[:n]
		r.apiKeySigningSecret = signingKeyStringData
	}

	return r.apiKeySigningSecret
}

// Returns the name that should be assigned to a child k8s resource according
// to which component of the Audit workflow the child resource belongs to.
func (r *AuditReconciler) componentName(auditName string, component kinds.WorkflowComponent) string {
	return fmt.Sprintf("%v-%v", auditName, component.ShortComponent)
}

// Returns the .metadata.labels for the Job resource.
func (r *AuditReconciler) runLabels(audit *dyffv1alpha1.Audit) map[string]string {
	return map[string]string{
		"dyff.io/account":   audit.Spec.Account,
		"dyff.io/workflow":  "audit",
		"dyff.io/step":      "run",
		"dyff.io/component": "run",
		"dyff.io/audit":     lastIdComponent(string(audit.Spec.Id)),
	}
}

// Returns the .metadata.labels for the ConfigMap resource.
func (r *AuditReconciler) configLabels(audit *dyffv1alpha1.Audit, component kinds.WorkflowComponent) map[string]string {
	return map[string]string{
		"dyff.io/account":   audit.Spec.Account,
		"dyff.io/workflow":  "audit",
		"dyff.io/step":      component.ShortComponent,
		"dyff.io/component": "config",
		"dyff.io/audit":     lastIdComponent(string(audit.Spec.Id)),
	}
}

// Create the ConfigMap for the inference and verification jobs.
func (r *AuditReconciler) createConfigMap(audit *dyffv1alpha1.Audit, component kinds.WorkflowComponent) *corev1.ConfigMap {
	var builder strings.Builder
	yml := printers.YAMLPrinter{}
	yml.PrintObj(audit, &builder)

	name := r.componentName(audit.Name, component)
	immutable := true
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.configLabels(audit, component),
			Name:      name,
			Namespace: audit.Namespace,
		},
		Immutable: &immutable,
		Data: map[string]string{
			"audit.yaml": builder.String(),
		},
	}
}

// Create the Job resource for the audit run.
func (r *AuditReconciler) createRunJob(audit *dyffv1alpha1.Audit, apiToken string) *batchv1.Job {
	const emptydirPath string = "/audit"
	const serverPort int32 = 8080

	// We need an lvalue for this later because EmptyDir takes SizeLimit as
	// a pointer
	tmpSizeLimit := resource.MustParse("4Gi")
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.runLabels(audit),
			Name:      r.componentName(audit.Name, kinds.AuditRun()),
			Namespace: audit.Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.runLabels(audit),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  r.componentName(audit.Name, kinds.AuditProxy()),
							Image: os.Getenv("DYFF_WORKFLOWS__COMMON__API_PROXY__IMAGE"),
							// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/dyff/dyff-api-proxy:latest",
							Env: []corev1.EnvVar{
								{
									Name:  "ALIGNMENTLABS_DYFF_API_ENDPOINT",
									Value: fmt.Sprintf("http://api-server.default.svc.cluster.local:%v/dyff/v0", serverPort),
								},
								{
									Name:  "ALIGNMENTLABS_DYFF_API_KEY",
									Value: apiToken,
								},
							},
							Args: []string{
								"--port",
								strconv.Itoa(int(serverPort)),
								"--log-level",
								"debug",
							},
							Ports: []corev1.ContainerPort{
								{ContainerPort: serverPort},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("8Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("8Gi"),
								},
							},
							Lifecycle: &corev1.Lifecycle{
								PostStart: &corev1.LifecycleHandler{
									Exec: &corev1.ExecAction{
										Command: []string{
											"/bin/sh", "-c",
											fmt.Sprintf("curl -sS --head -X GET --retry 6 --retry-connrefused --retry-delay 10 http://127.0.0.1:%v/health", serverPort),
										},
									},
								},
							},
						},
						{
							Name:  r.componentName(audit.Name, kinds.AuditRun()),
							Image: os.Getenv("DYFF_WORKFLOWS__AUDIT__RUN__IMAGE"),
							Env: []corev1.EnvVar{
								{
									Name:  "ALIGNMENTLABS_DYFF_API_ENDPOINT",
									Value: fmt.Sprintf("http://localhost:%v", serverPort),
								},
							},
							Args: []string{
								"--audit_yaml=/etc/config/audit.yaml",
								fmt.Sprintf("--jupyterbook_path=%v", emptydirPath),
							},
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "config",
									MountPath: "/etc/config",
								},
								{
									Name:      "tmp",
									MountPath: emptydirPath,
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":               resource.MustParse("1000m"),
									"memory":            resource.MustParse("8Gi"),
									"ephemeral-storage": resource.MustParse("8Gi"),
								},
								Limits: corev1.ResourceList{
									"cpu":               resource.MustParse("1000m"),
									"memory":            resource.MustParse("8Gi"),
									"ephemeral-storage": resource.MustParse("8Gi"),
								},
							},
						},
					},
					Volumes: []corev1.Volume{
						{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: r.componentName(audit.Name, kinds.AuditRun()),
									},
								},
							},
						},
						{
							Name: "tmp",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{
									SizeLimit: &tmpSizeLimit,
								},
							},
						},
					},
					RestartPolicy: "Never",
				},
			},
		},
	}
}

//+kubebuilder:rbac:groups=dyff.io,resources=audits,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=audits/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=audits/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=configmaps;services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Audit object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *AuditReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var err error
	var logger = log.FromContext(ctx)

	// Get the Audit
	var audit dyffv1alpha1.Audit
	err = r.Get(ctx, req.NamespacedName, &audit)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("Audit NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get Audit")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// Get child Jobs
	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	var runFailed bool = false
	for _, job := range failedJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == "run" {
			runFailed = true
		}
	}

	var runSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == "run" {
			runSucceeded = true
		}
	}

	if k8sMeta.IsStatusConditionTrue(audit.Status.Conditions, "Complete") ||
		k8sMeta.IsStatusConditionTrue(audit.Status.Conditions, "Failed") {
		// Audit has reached terminal state

		// Only delete the Jobs after Condition updates are visible, since we
		// use Job status as a cue for which step of the workflow we're in.
		err = r.Delete(ctx, r.createRunJob(&audit, ""), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	if runSucceeded || runFailed {
		// If run step reached terminal state, set status, clean up,
		// return ok.

		if runSucceeded {
			// Update status
			k8sMeta.SetStatusCondition(
				&audit.Status.Conditions,
				metav1.Condition{
					Type:               "Complete",
					Status:             metav1.ConditionTrue,
					Reason:             "Complete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Audit is complete",
				},
			)
			k8sMeta.SetStatusCondition(
				&audit.Status.Conditions,
				metav1.Condition{
					Type:               "Failed",
					Status:             metav1.ConditionFalse,
					Reason:             "Complete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Audit is complete",
				},
			)
			err = r.Status().Update(ctx, &audit)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("audit workflow succeeded")
		} else if runFailed {
			k8sMeta.SetStatusCondition(
				&audit.Status.Conditions,
				metav1.Condition{
					Type:               "Failed",
					Status:             metav1.ConditionTrue,
					Reason:             "RunFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Run Job failed",
				},
			)
			// Don't requeue (unless status update fails) -- if we want retry
			// semantics, specify that in the child Jobs
			err = r.Status().Update(ctx, &audit)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("audit workflow failed")
		}

		// Clean up resources from run step
		// AFAICT we need a full resource spec to call Delete()
		err = r.Delete(ctx, r.createConfigMap(&audit, kinds.AuditRun()))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
	} else {
		// Else no jobs have run, so start run, set status.

		var apiKey = generateAuditAPIKey(audit.Spec.Id)
		token, err := tokens.SignAPIKey(r.getSigningSecret(ctx), apiKey)
		if err != nil {
			logger.Error(err, "failed to sign APIKey")
			return ctrl.Result{}, err
		}

		config := r.createConfigMap(&audit, kinds.AuditRun())
		if err := ctrl.SetControllerReference(&audit, config, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt run ConfigMap")
			return ctrl.Result{}, err
		}
		runJob := r.createRunJob(&audit, token)
		if err := ctrl.SetControllerReference(&audit, runJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt run Job")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, config)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create run ConfigMap", "config", config)
			return ctrl.Result{}, err
		}
		err = r.Create(ctx, runJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create run Job", "runJob", runJob)
			return ctrl.Result{}, err
		}

		logger.Info("created workflow audit.run")

		k8sMeta.SetStatusCondition(
			&audit.Status.Conditions,
			metav1.Condition{
				Type:               "Complete",
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Audit is running",
			},
		)
		k8sMeta.SetStatusCondition(
			&audit.Status.Conditions,
			metav1.Condition{
				Type:               "Failed",
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Audit is running normally",
			},
		)
		if err := r.Status().Update(ctx, &audit); err != nil {
			return ctrl.Result{}, err
		}
	}

	// Return OK
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *AuditReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.Audit)

	// Cache Jobs whose owner is an Audit
	err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.Audit {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": "audit",
		},
	})
	if err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.Audit{}).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.ConfigMap{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
