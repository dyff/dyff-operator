// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package controller

import (
	"context"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/go-logr/logr"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/cli-runtime/pkg/printers"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
)

// ModelReconciler reconciles a Model object
type ModelReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	ownerKey string
}

type ModelReconcilerKernel struct {
	DyffReconcilerKernel

	Model      *dyffv1alpha1.Model
	reconciler *ModelReconciler
	ctx        context.Context
	logger     logr.Logger
}

func (r *ModelReconciler) newKernel(ctx context.Context, model *dyffv1alpha1.Model) *ModelReconcilerKernel {
	return &ModelReconcilerKernel{
		DyffReconcilerKernel: DyffReconcilerKernel{
			Workflow:             ModelWorkflowPlural(),
			Kind:                 kinds.Model,
			ShortName:            kinds.ModelShort,
			WorkflowSingleLetter: ModelWorkflowSingleLetter(),
		},
		Model:      model,
		reconciler: r,
		ctx:        ctx,
		logger:     log.FromContext(ctx),
	}
}

func ModelWorkflowPlural() string {
	return "models"
}

func ModelWorkflowSingleLetter() string {
	return "m"
}

func ModelStatusFetchSuccessful() string {
	return "models.dyff.io/FetchSuccessful"
}

func ModelStatusUploadSuccessful() string {
	return "models.dyff.io/UploadSuccessful"
}

func (k *ModelReconcilerKernel) artifactsLocalPath() string {
	return k.DyffReconcilerKernel.artifactsLocalPath(k.Model.Spec.Id)
}

func (k *ModelReconcilerKernel) componentName(component kinds.WorkflowComponent) string {
	return k.DyffReconcilerKernel.componentName(&k.Model.Spec.DyffEntity, component)
}

// Returns the .metadata.labels for the ConfigMap resource.
func (k *ModelReconcilerKernel) configLabels() map[string]string {
	return map[string]string{
		"dyff.io/account":   k.Model.Spec.Account,
		"dyff.io/workflow":  ModelWorkflowPlural(),
		"dyff.io/component": kinds.ModelConfig().Component,
		"dyff.io/entity":    lastIdComponent(string(k.Model.Spec.Id)),
	}
}

// Returns the .metadata.labels for the fetch Job resource.
func (k *ModelReconcilerKernel) fetchLabels() map[string]string {
	return map[string]string{
		"dyff.io/account":   k.Model.Spec.Account,
		"dyff.io/workflow":  ModelWorkflowPlural(),
		"dyff.io/step":      kinds.ModelFetch().Component,
		"dyff.io/component": kinds.ModelFetch().Component,
		"dyff.io/entity":    lastIdComponent(string(k.Model.Spec.Id)),
	}
}

// Create the ConfigMap for the model jobs.
func (k *ModelReconcilerKernel) getConfigMapManifest() *corev1.ConfigMap {
	var builder strings.Builder
	yml := printers.YAMLPrinter{}
	yml.PrintObj(k.Model, &builder)

	name := k.componentName(kinds.ModelConfig())
	immutable := true
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    k.configLabels(),
			Name:      name,
			Namespace: k.Model.Namespace,
		},
		Immutable: &immutable,
		Data: map[string]string{
			"model.yaml": builder.String(),
		},
	}
}

func (k *ModelReconcilerKernel) getArtifactsPersistentVolumeClaimManifest() *corev1.PersistentVolumeClaim {
	return k.DyffReconcilerKernel.getArtifactsPersistentVolumeClaimManifestForEntity(
		&k.Model.ObjectMeta, &k.Model.Spec.DyffEntity, k.Model.Spec.Storage.Quantity,
	)
}

func (k *ModelReconcilerKernel) getArtifactsUploadJobManifest() *batchv1.Job {
	return k.DyffReconcilerKernel.getArtifactsUploadJobManifest(
		kinds.Model, &k.Model.ObjectMeta, &k.Model.Spec.DyffEntity,
	)
}

// Create the Job resource for the model build.
func (k *ModelReconcilerKernel) getFetchJobManifest() (*batchv1.Job, error) {
	var modelVolumeName string = k.getArtifactsPersistentVolumeClaimName(&k.Model.Spec.DyffEntity)
	var modelVolume corev1.Volume
	var modelVolumeMountPath string
	var annotations map[string]string = make(map[string]string)

	// FIXME: Inconsistent naming ("ObjectStore" here vs. "ObjectStorage" in Dyff schema)
	if k.Model.Spec.Storage.Kind == "ObjectStore" || k.Model.Spec.Storage.Kind == "PersistentVolume" {
		modelVolume = corev1.Volume{
			Name: modelVolumeName,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: modelVolumeName,
				},
			},
		}
		modelVolumeMountPath = k.artifactsVolumeMountPath()
	} else if k.Model.Spec.Storage.Kind == "FUSEVolume" {
		storageUrl, err := url.Parse(artifactStorageUrl("Model", k.Model.Spec.Id))
		if err != nil {
			return nil, err
		}
		bucketName := storageUrl.Hostname()
		if bucketName == "" {
			return nil, fmt.Errorf("no bucket name in URL: %v", storageUrl)
		}
		bucketPath := strings.Trim(storageUrl.Path, "/")
		if bucketPath == "" {
			return nil, fmt.Errorf("no artifact path in URL: %v", storageUrl)
		}
		// TODO: This FUSE CSI config is gcloud-specific
		// https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/cloud-storage-fuse-csi-driver
		// https://github.com/GoogleCloudPlatform/gcs-fuse-csi-driver/issues/153
		annotations["gke-gcsfuse/volumes"] = "true"
		modelVolume = corev1.Volume{
			Name: modelVolumeName,
			VolumeSource: corev1.VolumeSource{
				// https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/cloud-storage-fuse-csi-driver#deploy-pod
				CSI: &corev1.CSIVolumeSource{
					Driver: "gcsfuse.csi.storage.gke.io",
					VolumeAttributes: map[string]string{
						"bucketName": bucketName,
						// Mount only the artifact sub-directory, for security
						// Non-root group/user must be set:
						// https://github.com/GoogleCloudPlatform/gcs-fuse-csi-driver/blob/main/docs/troubleshooting.md#io-errors-in-your-workloads
						"mountOptions": fmt.Sprintf(
							"implicit-dirs,only-dir=%v,gid=%v,uid=%v",
							bucketPath,
							ORDINARY_GROUP,
							ORDINARY_USER,
						),
					},
				},
			},
		}
		// We're only mounting the model subdirectory from the bucket, so we
		// want to mount it at /.../model_id
		modelVolumeMountPath = k.artifactsLocalPath()
	} else {
		return nil, nil
	}

	var fetchJob = &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    k.fetchLabels(),
			Name:      k.componentName(kinds.ModelFetch()),
			Namespace: k.Model.Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels:      k.fetchLabels(),
					Annotations: annotations,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  k.componentName(kinds.ModelFetch()),
							Image: os.Getenv("DYFF_WORKFLOWS__MODELS__FETCH__IMAGE"),
							// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/dyff/fetch-model:latest",
							Args: []string{
								"--model-yaml=/etc/config/model.yaml",
								fmt.Sprintf("--local-dir=%v", k.artifactsLocalPath()),
							},
							EnvFrom: []corev1.EnvFromSource{
								{
									SecretRef: &corev1.SecretEnvSource{
										LocalObjectReference: corev1.LocalObjectReference{
											Name: "model-fetcher-external-credentials",
										},
									},
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"cpu":    resource.MustParse("1000m"),
									"memory": resource.MustParse("16Gi"),
								},
								Limits: corev1.ResourceList{
									"memory": resource.MustParse("16Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "config",
									MountPath: "/etc/config",
								},
								{
									Name:      modelVolumeName,
									MountPath: modelVolumeMountPath,
								},
							},
						},
					},
					SecurityContext: workflowsPodSecurityContext(),
					Volumes: []corev1.Volume{
						{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: k.componentName(kinds.ModelConfig()),
									},
								},
							},
						},
						modelVolume,
					},
					InitContainers: initContainers(),
					RestartPolicy:  "Never",
					// TODO: Make configurable
					ServiceAccountName: "model-fetcher",
				},
			},
		},
	}

	return fetchJob, nil
}

func (k *ModelReconcilerKernel) onUploadSucceeded() (result ctrl.Result, err error) {
	k8sMeta.SetStatusCondition(
		&k.Model.Status.Conditions,
		metav1.Condition{
			Type:               ModelStatusUploadSuccessful(),
			Status:             metav1.ConditionTrue,
			Reason:             "UploadStepComplete",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Upload step is Complete.",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.Model)
	if err != nil {
		return ctrl.Result{}, err
	}
	k.logger.Info("model workflow succeeded")

	return ctrl.Result{}, nil
}

func (k *ModelReconcilerKernel) onUploadFailed() (result ctrl.Result, err error) {
	k8sMeta.SetStatusCondition(
		&k.Model.Status.Conditions,
		metav1.Condition{
			Type:               ModelStatusUploadSuccessful(),
			Status:             metav1.ConditionFalse,
			Reason:             "UploadStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Upload step Failed.",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.Model)
	if err != nil {
		return ctrl.Result{}, err
	}
	k.logger.Info("model workflow failed (Upload)")

	return ctrl.Result{}, nil
}

func (k *ModelReconcilerKernel) onFetchSucceeded() (result ctrl.Result, err error) {
	k8sMeta.SetStatusCondition(
		&k.Model.Status.Conditions,
		metav1.Condition{
			Type:               ModelStatusFetchSuccessful(),
			Status:             metav1.ConditionTrue,
			Reason:             "FetchStepComplete",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Fetch step is Complete",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.Model)
	if err != nil {
		return ctrl.Result{}, err
	}

	if k.needsUpload() {
		return k.createUploadJob()
	} else {
		return ctrl.Result{}, nil
	}
}

func (k *ModelReconcilerKernel) onFetchFailed() (result ctrl.Result, err error) {
	k8sMeta.SetStatusCondition(
		&k.Model.Status.Conditions,
		metav1.Condition{
			Type:               ModelStatusFetchSuccessful(),
			Status:             metav1.ConditionFalse,
			Reason:             "FetchStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Fetch step Failed",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.Model)
	if err != nil {
		return ctrl.Result{}, err
	}
	k.logger.Info("model workflow failed (fetch)")

	// Always Upload, even after failure, to preserve logs
	return k.createUploadJob()
}

func (k *ModelReconcilerKernel) onCreate() (result ctrl.Result, err error) {
	result, err = k.createFetchJob()
	if err != nil {
		return result, err
	}

	k8sMeta.SetStatusCondition(
		&k.Model.Status.Conditions,
		metav1.Condition{
			Type:               StatusReady,
			Status:             metav1.ConditionFalse,
			Reason:             "Running",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "Model workflow is running.",
		},
	)
	k8sMeta.SetStatusCondition(
		&k.Model.Status.Conditions,
		metav1.Condition{
			Type:               StatusError,
			Status:             metav1.ConditionFalse,
			Reason:             "Running",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "Model workflow is running.",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.Model)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (k *ModelReconcilerKernel) createUploadJob() (result ctrl.Result, err error) {
	uploadJob := k.getArtifactsUploadJobManifest()
	if err := ctrl.SetControllerReference(k.Model, uploadJob, k.reconciler.Scheme); err != nil {
		k.logger.Error(err, "failed to adopt upload Job")
		return ctrl.Result{}, err
	}

	err = k.reconciler.Create(k.ctx, uploadJob)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create upload Job", "uploadJob", uploadJob)
		return ctrl.Result{}, err
	}

	k.logger.Info("model workflow step: Upload")

	return ctrl.Result{}, nil
}

func (k *ModelReconcilerKernel) createFetchJob() (result ctrl.Result, err error) {
	// Create PVC for the model
	pvc := k.getArtifactsPersistentVolumeClaimManifest()
	// Note: We *do not* adopt the PVC because we don't want its
	// lifetime tied to the lifetime of the Model resource
	// FIXME: I think the way forward is to set a finalizer on the
	// Model object, then remove the controller references to persistent
	// objects in the finalizer so that they don't get GC'd.
	// See: https://sdk.operatorframework.io/docs/building-operators/golang/advanced-topics/#external-resources
	err = k.reconciler.Create(k.ctx, pvc)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create model PersistentVolumeClaim", "storage", pvc)
		return ctrl.Result{}, err
	}

	config := k.getConfigMapManifest()
	if err := ctrl.SetControllerReference(k.Model, config, k.reconciler.Scheme); err != nil {
		k.logger.Error(err, "failed to adopt fetch ConfigMap")
		return ctrl.Result{}, err
	}
	fetchJob, err := k.getFetchJobManifest()
	if err != nil {
		return ctrl.Result{}, err
	}
	if err := ctrl.SetControllerReference(k.Model, fetchJob, k.reconciler.Scheme); err != nil {
		k.logger.Error(err, "failed to adopt fetch Job")
		return ctrl.Result{}, err
	}

	err = k.reconciler.Create(k.ctx, config)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create fetch ConfigMap", "config", config)
		return ctrl.Result{}, err
	}
	err = k.reconciler.Create(k.ctx, fetchJob)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create fetch Job", "fetchJob", fetchJob)
		return ctrl.Result{}, err
	}

	k.logger.Info("model workflow step: Fetch")

	return ctrl.Result{}, nil
}

// ---------------------------------------------------------------------------
// These remaining methods implement the public Kernel interface that will be
// used by the generic reconciler.

func (k *ModelReconcilerKernel) step(
	succeededJobs []*batchv1.Job,
	failedJobs []*batchv1.Job,
) (result ctrl.Result, err error) {
	var fetchFailed bool = false
	var uploadFailed bool = false
	for _, job := range failedJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.ModelFetch().Component {
			fetchFailed = true
		} else if step == k.ArtifactsUpload().Component {
			uploadFailed = true
		}
	}

	var fetchSucceeded bool = false
	var uploadSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.ModelFetch().Component {
			fetchSucceeded = true
		} else if step == k.ArtifactsUpload().Component {
			uploadSucceeded = true
		}
	}

	if uploadSucceeded {
		return k.onUploadSucceeded()
	} else if uploadFailed {
		return k.onUploadFailed()
	} else if fetchSucceeded {
		return k.onFetchSucceeded()
	} else if fetchFailed {
		return k.onFetchFailed()
	} else {
		return k.onCreate()
	}
}

func (k *ModelReconcilerKernel) needsUpload() bool {
	// FIXME: Inconsistent naming ("ObjectStore" here vs. "ObjectStorage" in Dyff schema)
	return k.Model.Spec.Storage.Kind == "ObjectStore"
}

// If the workflow has reached a terminal Condition, return that Condition. The
// operator should clean up any resources and set the returned Condition in the
// workflow k8s manifest.
//
// Else, return nil. The operator should let the workflow continue running.
func (k *ModelReconcilerKernel) terminalCondition() *metav1.Condition {
	var conditions []metav1.Condition = k.Model.Status.Conditions
	var rootCause *metav1.Condition
	var condition *metav1.Condition

	// If top-level terminal condition is already set, return it
	for _, conditionType := range []string{StatusReady, StatusError} {
		condition = k8sMeta.FindStatusCondition(conditions, conditionType)
		if condition != nil && condition.Status == metav1.ConditionTrue {
			return condition
		}
	}

	condition = k8sMeta.FindStatusCondition(conditions, ModelStatusFetchSuccessful())
	if condition == nil || condition.Status == metav1.ConditionUnknown {
		return nil
	} else if condition.Status == metav1.ConditionFalse {
		// If Fetch failed, we still want to Upload the log data [*]
		rootCause = &metav1.Condition{
			Type:               StatusError,
			Status:             metav1.ConditionTrue,
			Reason:             "FetchStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Fetch step Failed.",
		}
	}

	if k.needsUpload() {
		condition = k8sMeta.FindStatusCondition(conditions, ModelStatusUploadSuccessful())
		if condition == nil || condition.Status == metav1.ConditionUnknown {
			return nil
		}
	}

	// [*] Now we can report Failed
	// Even if Upload failed, too, it's more important that the user know that
	// the Run step failed, and we can only report one status.
	if rootCause != nil {
		return rootCause
	}

	if k.needsUpload() {
		if condition.Status == metav1.ConditionFalse {
			// Fail immediately if Upload failed
			return &metav1.Condition{
				Type:               StatusError,
				Status:             metav1.ConditionTrue,
				Reason:             "UploadStepFailed",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "The Upload step Failed.",
			}
		}
	}

	// Everything succeeded
	return &metav1.Condition{
		Type:               StatusReady,
		Status:             metav1.ConditionTrue,
		Reason:             StatusReady,
		LastTransitionTime: metav1.NewTime(time.Now()),
		Message:            "Model is Ready.",
	}
}

func (k *ModelReconcilerKernel) cleanup() (ctrl.Result, error) {
	fetchJob, _ := k.getFetchJobManifest()
	objects := []client.Object{
		k.getConfigMapManifest(),
		fetchJob,
		k.getArtifactsUploadJobManifest(),
	}

	success := isStatusConditionSuccess(k.Model.Status.Conditions)
	if !success || k.Model.Spec.Storage.Kind != "PersistentVolume" {
		objects = append(objects, k.getArtifactsPersistentVolumeClaimManifest())
	}

	for _, manifest := range objects {
		if manifest != nil {
			err := k.reconciler.Delete(k.ctx, manifest, client.PropagationPolicy("Background"))
			if client.IgnoreNotFound(err) != nil {
				return ctrl.Result{}, err
			}
		}
	}
	return ctrl.Result{}, nil
}

//+kubebuilder:rbac:groups=dyff.io,resources=models,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=models/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=models/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=configmaps;persistentvolumeclaims;services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=snapshot.storage.k8s.io,resources=volumesnapshots,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Model object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *ModelReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var err error
	var logger = log.FromContext(ctx)

	// Get the Model
	var model dyffv1alpha1.Model
	err = r.Get(ctx, req.NamespacedName, &model)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("Model NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get Model")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	kernel := r.newKernel(ctx, &model)

	// If in a terminal condition, set condition, clean up resources, return
	if terminal := kernel.terminalCondition(); terminal != nil {
		logger.Info("terminal status", "status", terminal)

		k8sMeta.SetStatusCondition(&model.Status.Conditions, *terminal)
		err = r.Status().Update(ctx, &model)
		if err != nil {
			return ctrl.Result{}, err
		}

		return kernel.cleanup()
	}

	// Get child Jobs
	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	return kernel.step(succeededJobs, failedJobs)
}

// SetupWithManager sets up the controller with the Manager.
func (r *ModelReconciler) SetupWithManager(mgr ctrl.Manager) error {
	var err error
	var logger = mgr.GetLogger()

	// Register the VolumeSnapshot unstructured type
	// See: https://github.com/operator-framework/operator-sdk/issues/1630#issuecomment-507917573
	volumeSnapshotGroupVersionKind := schema.GroupVersionKind{
		Group:   "snapshot.storage.k8s.io",
		Version: "v1",
		Kind:    "VolumeSnapshot",
	}
	scheme := mgr.GetScheme()
	_, err = scheme.New(volumeSnapshotGroupVersionKind)
	if runtime.IsNotRegisteredError(err) {
		// Register the GVK with the schema
		scheme.AddKnownTypeWithName(volumeSnapshotGroupVersionKind, &unstructured.Unstructured{})
		metav1.AddToGroupVersion(mgr.GetScheme(), schema.GroupVersion{
			Group:   volumeSnapshotGroupVersionKind.Group,
			Version: volumeSnapshotGroupVersionKind.Version,
		})
	} else if err != nil {
		logger.Error(err, "")
		os.Exit(1)
	}

	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.Model)

	// Cache Jobs whose owner is a Model
	err = mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.Model {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": ModelWorkflowPlural(),
		},
	})
	if err != nil {
		return err
	}

	// Note: The Model controller creates PVCs but *does not* own them because
	// we don't want their lifetime tied to the lifetime of the Model resource
	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.Model{}).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.ConfigMap{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
