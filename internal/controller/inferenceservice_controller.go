// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package controller

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/cli-runtime/pkg/printers"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
)

// InferenceServiceReconciler reconciles a InferenceService object
type InferenceServiceReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	ownerKey string
}

// Returns the name that should be assigned to a child k8s resource according
// to which component of the InferenceService workflow the child resource belongs to.
func (r *InferenceServiceReconciler) componentName(inferenceserviceName string, component kinds.WorkflowComponent) string {
	return fmt.Sprintf("%v-%v", inferenceserviceName, component.ShortComponent)
}

// Returns the .metadata.labels for the build Job resource.
func (r *InferenceServiceReconciler) buildLabels(inferenceService *dyffv1alpha1.InferenceService) map[string]string {
	return map[string]string{
		"dyff.io/account":          inferenceService.Spec.Account,
		"dyff.io/workflow":         "inferenceservice",
		"dyff.io/step":             "build",
		"dyff.io/component":        "build",
		"dyff.io/inferenceservice": lastIdComponent(string(inferenceService.Spec.Id)),
	}
}

// Returns the .metadata.labels for the ConfigMap resource.
func (r *InferenceServiceReconciler) configLabels(inferenceService *dyffv1alpha1.InferenceService, component kinds.WorkflowComponent) map[string]string {
	return map[string]string{
		"dyff.io/account":          inferenceService.Spec.Account,
		"dyff.io/workflow":         "inferenceservice",
		"dyff.io/step":             component.ShortComponent,
		"dyff.io/component":        "config",
		"dyff.io/inferenceservice": lastIdComponent(string(inferenceService.Spec.Id)),
	}
}

// Create the ConfigMap for the inference and verification jobs.
func (r *InferenceServiceReconciler) createConfigMap(inferenceservice *dyffv1alpha1.InferenceService, component kinds.WorkflowComponent) *corev1.ConfigMap {
	var builder strings.Builder
	yml := printers.YAMLPrinter{}
	yml.PrintObj(inferenceservice, &builder)

	name := r.componentName(inferenceservice.Name, component)
	immutable := true
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.configLabels(inferenceservice, component),
			Name:      name,
			Namespace: inferenceservice.Namespace,
		},
		Immutable: &immutable,
		Data: map[string]string{
			"inferenceservice.yaml": builder.String(),
		},
	}
}

// Create the Job resource for the inferenceservice build.
func (r *InferenceServiceReconciler) createBuildJob(inferenceservice *dyffv1alpha1.InferenceService) *batchv1.Job {
	const emptyDirPath string = "/tmp"
	// We need an lvalue for this later because EmptyDir takes SizeLimit as
	// a pointer
	tmpSizeLimit := resource.MustParse("32Gi")

	var imageName string = fmt.Sprintf(
		os.Getenv("DYFF_WORKFLOWS__INFERENCESERVICES__BUILD__IMAGE_TEMPLATE"),
		inferenceservice.Spec.PackagingFormat,
	)

	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.buildLabels(inferenceservice),
			Name:      r.componentName(inferenceservice.Name, kinds.InferenceServiceBuild()),
			Namespace: inferenceservice.Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: r.buildLabels(inferenceservice),
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  r.componentName(inferenceservice.Name, kinds.InferenceServiceBuild()),
							Image: imageName,
							Args: []string{
								"--inference_service_yaml=/etc/config/inferenceservice.yaml",
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									"ephemeral-storage": resource.MustParse("64Gi"),
									"memory":            resource.MustParse("32Gi"),
								},
								Limits: corev1.ResourceList{
									"ephemeral-storage": resource.MustParse("64Gi"),
									"memory":            resource.MustParse("64Gi"),
								},
							},
							SecurityContext: workflowContainerSecurityContext(),
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "config",
									MountPath: "/etc/config",
								},
								{
									Name:      "tmp",
									MountPath: emptyDirPath,
								},
							},
						},
					},
					Volumes: []corev1.Volume{
						{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: r.componentName(inferenceservice.Name, kinds.InferenceServiceBuild()),
									},
								},
							},
						},
						{
							Name: "tmp",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{
									SizeLimit: &tmpSizeLimit,
								},
							},
						},
					},
					InitContainers:     initContainers(),
					RestartPolicy:      "Never",
					ServiceAccountName: "inference-service-builder",
				},
			},
		},
	}
}

//+kubebuilder:rbac:groups=dyff.io,resources=inferenceservices,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=inferenceservices/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=inferenceservices/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=configmaps;services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the InferenceService object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *InferenceServiceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var err error
	var logger = log.FromContext(ctx)

	// Get the InferenceService
	var inferenceservice dyffv1alpha1.InferenceService
	err = r.Get(ctx, req.NamespacedName, &inferenceservice)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("InferenceService NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get InferenceService")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// Get child Jobs
	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	var buildFailed bool = false
	for _, job := range failedJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.InferenceServiceBuild().ShortComponent {
			buildFailed = true
		}
	}

	var buildSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == kinds.InferenceServiceBuild().ShortComponent {
			buildSucceeded = true
		}
	}

	if k8sMeta.IsStatusConditionTrue(inferenceservice.Status.Conditions, "Ready") ||
		k8sMeta.IsStatusConditionTrue(inferenceservice.Status.Conditions, "Error") {
		// InferenceService has reached terminal state

		// Only delete the Jobs after Condition updates are visible, since we
		// use Job status as a cue for which step of the workflow we're in.
		err = r.Delete(ctx, r.createBuildJob(&inferenceservice), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	if buildSucceeded || buildFailed {
		// If build step reached terminal state, set status, clean up,
		// return ok.

		if buildSucceeded {
			// Update status
			k8sMeta.SetStatusCondition(
				&inferenceservice.Status.Conditions,
				metav1.Condition{
					Type:               "Ready",
					Status:             metav1.ConditionTrue,
					Reason:             "Ready",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "InferenceService is ready",
				},
			)
			k8sMeta.SetStatusCondition(
				&inferenceservice.Status.Conditions,
				metav1.Condition{
					Type:               "Error",
					Status:             metav1.ConditionFalse,
					Reason:             "Ready",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "InferenceService is ready",
				},
			)
			err = r.Status().Update(ctx, &inferenceservice)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("inferenceservice workflow succeeded")
		} else if buildFailed {
			k8sMeta.SetStatusCondition(
				&inferenceservice.Status.Conditions,
				metav1.Condition{
					Type:               "Error",
					Status:             metav1.ConditionTrue,
					Reason:             "BuildFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Build Job failed",
				},
			)
			// Don't requeue (unless status update fails) -- if we want retry
			// semantics, specify that in the child Jobs
			err = r.Status().Update(ctx, &inferenceservice)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("inferenceservice workflow failed")
		}

		// Clean up resources from build step
		// AFAICT we need a full resource spec to call Delete()
		err = r.Delete(ctx, r.createConfigMap(&inferenceservice, kinds.InferenceServiceBuild()))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
	} else {
		// Else no jobs have run, so start build, set status.

		config := r.createConfigMap(&inferenceservice, kinds.InferenceServiceBuild())
		if err := ctrl.SetControllerReference(&inferenceservice, config, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt build ConfigMap")
			return ctrl.Result{}, err
		}
		buildJob := r.createBuildJob(&inferenceservice)
		if err := ctrl.SetControllerReference(&inferenceservice, buildJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt build Job")
			return ctrl.Result{}, err
		}

		err = r.Create(ctx, config)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create build ConfigMap", "config", config)
			return ctrl.Result{}, err
		}
		err = r.Create(ctx, buildJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create build Job", "buildJob", buildJob)
			return ctrl.Result{}, err
		}

		logger.Info("created workflow inferenceservice.build")

		k8sMeta.SetStatusCondition(
			&inferenceservice.Status.Conditions,
			metav1.Condition{
				Type:               "Ready",
				Status:             metav1.ConditionFalse,
				Reason:             "Building",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "InferenceService is building",
			},
		)
		k8sMeta.SetStatusCondition(
			&inferenceservice.Status.Conditions,
			metav1.Condition{
				Type:               "Error",
				Status:             metav1.ConditionFalse,
				Reason:             "Building",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "InferenceService is building normally",
			},
		)
		if err := r.Status().Update(ctx, &inferenceservice); err != nil {
			return ctrl.Result{}, err
		}
	}

	// Return OK
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *InferenceServiceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.InferenceService)

	// Cache Jobs whose owner is an InferenceService
	err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.InferenceService {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": "inferenceservice",
		},
	})
	if err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.InferenceService{}).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.ConfigMap{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
