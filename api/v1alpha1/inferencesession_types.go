// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type MountPathSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Name string `json:"name"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	MountPath string `json:"mountPath"`
}

type ReadOnlyVolumeSpec struct {
	MountPathSpec `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ClaimName string `json:"claimName"`
}

type EphemeralVolumeSpec struct {
	MountPathSpec `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Storage resource.Quantity `json:"storage"`
}

type FUSEVolumeSpec struct {
	MountPathSpec `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Artifact NamespacedID `json:"artifact"`
}

type ArtifactsVolumeSpec struct {
	MountPathSpec `json:",inline"`

	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinItems=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Artifacts []NamespacedID `json:"artifacts"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Storage resource.Quantity `json:"storage"`
}

type InferenceServiceDependencySpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	EphemeralVolume *EphemeralVolumeSpec `json:"ephemeralVolume,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	FUSEVolume *FUSEVolumeSpec `json:"fuseVolume,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ReadOnlyVolume *ReadOnlyVolumeSpec `json:"readOnlyVolume,omitempty"`
}

type AcceleratorGPUSpec struct {
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinItems=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	HardwareTypes []string `json:"hardwareTypes"`

	// +kubebuilder:default=1
	// +kubebuilder:validation:Minimum=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Count int32 `json:"count"`

	// [DEPRECATED] Use .count if more than one GPU is required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Memory *resource.Quantity `json:"memory,omitempty"`
}

type AcceleratorSpec struct {
	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Kind string `json:"kind"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Gpu *AcceleratorGPUSpec `json:"gpu,omitempty"`
}

type InferenceSessionTemplate struct {
	SimpleContainer `json:",inline"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	ArtifactsVolume *ArtifactsVolumeSpec `json:"artifactsVolume,omitempty"`

	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Dependencies []InferenceServiceDependencySpec `json:"dependencies,omitempty"`

	// Accelerator hardware to use *for each node*. For example, if you specify
	// 2 GPUs in the accelerator spec, and set .nodes = 2, then the session will
	// require a total of 4 GPUs.
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Accelerator *AcceleratorSpec `json:"accelerator,omitempty"`

	// +kubebuilder:default=1
	// +kubebuilder:validation:Minimum=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Replicas int32 `json:"replicas,omitempty"`

	// +kubebuilder:default=true
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	UseSpotPods bool `json:"useSpotPods"`

	// How many nodes to use for each replica.
	// +kubebuilder:default=1
	// +kubebuilder:validation:Minimum=1
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Nodes int32 `json:"nodes,omitempty"`
}

// InferenceSessionSpec defines the desired state of InferenceSession
type InferenceSessionSpec struct {
	DyffEntity `json:",inline"`

	InferenceSessionTemplate `json:",inline"`
}

// InferenceSessionStatus defines the observed state of InferenceSession
type InferenceSessionStatus struct {
	// +operator-sdk:csv:customresourcedefinitions:type=status
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// InferenceSession is the Schema for the inferencesessions API
type InferenceSession struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   InferenceSessionSpec   `json:"spec,omitempty"`
	Status InferenceSessionStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// InferenceSessionList contains a list of InferenceSession
type InferenceSessionList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []InferenceSession `json:"items"`
}

func init() {
	SchemeBuilder.Register(&InferenceSession{}, &InferenceSessionList{})
}
