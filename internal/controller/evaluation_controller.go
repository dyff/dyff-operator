// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package controller

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/cli-runtime/pkg/printers"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
)

// EvaluationReconciler reconciles a Evaluation object
type EvaluationReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	ownerKey string
}

func EvaluationWorkflowPlural() string {
	return "evaluations"
}

func EvaluationWorkflowSingleLetter() string {
	return "e"
}

// Returns the name that should be assigned to a child k8s resource according
// to which component of the Evaluation workflow the child resource belongs to.
func (r *EvaluationReconciler) componentName(evaluation *dyffv1alpha1.Evaluation, component kinds.WorkflowComponent) string {
	return fmt.Sprintf("e%v%v", evaluation.Spec.Id, component.SingleLetterComponent)
}

// Returns the .metadata.labels for the ConfigMap resource.
func (r *EvaluationReconciler) configLabels(evaluation *dyffv1alpha1.Evaluation, component kinds.WorkflowComponent) map[string]string {
	return map[string]string{
		"dyff.io/account":   evaluation.Spec.Account,
		"dyff.io/workflow":  EvaluationWorkflowPlural(),
		"dyff.io/step":      component.ShortComponent,
		"dyff.io/component": "config",
		"dyff.io/id":        lastIdComponent(string(evaluation.Spec.Id)),
	}
}

// Returns the .metadata.labels for the evaluation client Job.
func (r *EvaluationReconciler) clientLabels(evaluation *dyffv1alpha1.Evaluation) map[string]string {
	return map[string]string{
		"dyff.io/account":   evaluation.Spec.Account,
		"dyff.io/workflow":  EvaluationWorkflowPlural(),
		"dyff.io/step":      "inference",
		"dyff.io/component": "client",
		"dyff.io/id":        lastIdComponent(string(evaluation.Spec.Id)),
	}
}

// Returns the .metadata.labels for the inference components.
func (r *EvaluationReconciler) inferenceLabels(evaluation *dyffv1alpha1.Evaluation) map[string]string {
	return map[string]string{
		"dyff.io/account":   evaluation.Spec.Account,
		"dyff.io/workflow":  EvaluationWorkflowPlural(),
		"dyff.io/step":      "inference",
		"dyff.io/component": "session",
		"dyff.io/id":        lastIdComponent(string(evaluation.Spec.Id)),
	}
}

func (r *EvaluationReconciler) verificationLabels(evaluation *dyffv1alpha1.Evaluation) map[string]string {
	return map[string]string{
		"dyff.io/account":   evaluation.Spec.Account,
		"dyff.io/workflow":  EvaluationWorkflowPlural(),
		"dyff.io/step":      "verification",
		"dyff.io/component": "verification",
		"dyff.io/id":        lastIdComponent(string(evaluation.Spec.Id)),
	}
}

func (r *EvaluationReconciler) hasDependentInferenceSession(evaluation *dyffv1alpha1.Evaluation) bool {
	return evaluation.Spec.InferenceSessionReference == nil
}

// Create the ConfigMap for the inference and verification jobs.
func (r *EvaluationReconciler) createConfigMap(evaluation *dyffv1alpha1.Evaluation, component kinds.WorkflowComponent) *corev1.ConfigMap {
	var builder strings.Builder
	yml := printers.YAMLPrinter{}
	yml.PrintObj(evaluation, &builder)

	name := r.componentName(evaluation, component)
	immutable := true
	return &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.configLabels(evaluation, component),
			Name:      name,
			Namespace: evaluation.Namespace,
		},
		Immutable: &immutable,
		Data: map[string]string{
			"evaluation.yaml": builder.String(),
		},
	}
}

// Create the Job resource for the evaluation client.
func (r *EvaluationReconciler) createClientJob(evaluation *dyffv1alpha1.Evaluation) *batchv1.Job {
	name := r.componentName(evaluation, kinds.EvaluationClient())
	// Client contacts inference session at a different origin depending on
	// whether the session is managed by the Evaluation. Note that the k8s
	// Service associated with the session has the same name as the session.
	var sessionOrigin string
	var sessionName string
	if evaluation.Spec.InferenceSessionReference != nil {
		// Independent sessions are named like 'i<session-id>i'
		// FIXME: We should be calling a common function to get this name
		sessionName = fmt.Sprintf("i%vi", *evaluation.Spec.InferenceSessionReference)
	} else {
		// Dependent sessions are named like Evaluation components ('e<eval-id>i')
		sessionName = r.componentName(evaluation, kinds.EvaluationInference())
	}
	sessionOrigin = fmt.Sprintf(
		"http://%v.%v:80",
		sessionName,
		evaluation.Namespace,
	)
	// Failures are much more likely to be due to user error (e.g., bad data)
	// than recoverable system errors, so we only retry once.
	var backoffLimit int32 = 1
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.clientLabels(evaluation),
			Name:      name,
			Namespace: evaluation.Namespace,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &backoffLimit,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels:    r.clientLabels(evaluation),
					Namespace: evaluation.Namespace,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Name:  name,
						Image: os.Getenv("DYFF_WORKFLOWS__EVALUATIONS__CLIENT__IMAGE"),
						// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/dyff/evaluation-client:latest",
						Args: []string{
							"--evaluation_yaml=/etc/config/evaluation.yaml",
							fmt.Sprintf("--inference_session_origin=%v", sessionOrigin),
						},
						EnvFrom: []corev1.EnvFromSource{
							{
								ConfigMapRef: &corev1.ConfigMapEnvSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: "workflows-config",
									},
								},
							},
							{
								SecretRef: &corev1.SecretEnvSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: "workflows-storage-credentials",
									},
								},
							},
						},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								"cpu":    resource.MustParse("2000m"),
								"memory": resource.MustParse("16Gi"),
							},
							Limits: corev1.ResourceList{
								"cpu":    resource.MustParse("2000m"),
								"memory": resource.MustParse("16Gi"),
							},
						},
						SecurityContext: workflowContainerSecurityContext(),
						VolumeMounts: []corev1.VolumeMount{{
							Name:      "config",
							MountPath: "/etc/config",
						}},
					}},
					Volumes: []corev1.Volume{{
						Name: "config",
						VolumeSource: corev1.VolumeSource{
							ConfigMap: &corev1.ConfigMapVolumeSource{
								LocalObjectReference: corev1.LocalObjectReference{
									Name: name,
								},
							},
						},
					}},
					InitContainers:     initContainers(),
					RestartPolicy:      "Never",
					ServiceAccountName: "evaluation-client",
				},
			},
		},
	}
}

// Create the Deployment resource for the InferenceService.
func (r *EvaluationReconciler) createInferenceSession(evaluation *dyffv1alpha1.Evaluation) *dyffv1alpha1.InferenceSession {
	// The session is owned by the evaluation, so it gets the null ID
	spec := dyffv1alpha1.InferenceSessionSpec{
		DyffEntity:               dyffv1alpha1.DyffEntity{Id: dyffv1alpha1.NullId, Account: evaluation.Spec.Account},
		InferenceSessionTemplate: *evaluation.Spec.InferenceSession.DeepCopy(),
	}

	return &dyffv1alpha1.InferenceSession{
		ObjectMeta: metav1.ObjectMeta{
			Labels: r.inferenceLabels(evaluation),
			// We only have 2 free characters in the name, and one is already
			// used for the leading 'e', so we need to leave the suffix
			// character free for InferenceSession to use
			Name:      evaluation.Name,
			Namespace: evaluation.Namespace,
		},
		Spec: spec,
	}
}

// Create the Job resource for the verification process.
func (r *EvaluationReconciler) createVerificationJob(evaluation *dyffv1alpha1.Evaluation) *batchv1.Job {
	name := r.componentName(evaluation, kinds.EvaluationVerification())
	return &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    r.verificationLabels(evaluation),
			Name:      name,
			Namespace: evaluation.Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels:    r.verificationLabels(evaluation),
					Namespace: evaluation.Namespace,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Name:  name,
						Image: os.Getenv("DYFF_WORKFLOWS__EVALUATIONS__VERIFICATION__IMAGE"),
						// Image: "us-central1-docker.pkg.dev/dyff-354017/dyff-system/dyff/verify-evaluation-output:latest",
						Args: []string{"--evaluation_yaml=/etc/config/evaluation.yaml"},
						EnvFrom: []corev1.EnvFromSource{
							{
								ConfigMapRef: &corev1.ConfigMapEnvSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: "workflows-config",
									},
								},
							},
							{
								SecretRef: &corev1.SecretEnvSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: "workflows-storage-credentials",
									},
								},
							},
						},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								"cpu":    resource.MustParse("2000m"),
								"memory": resource.MustParse("16Gi"),
							},
							Limits: corev1.ResourceList{
								"cpu":    resource.MustParse("2000m"),
								"memory": resource.MustParse("16Gi"),
							},
						},
						SecurityContext: workflowContainerSecurityContext(),
						VolumeMounts: []corev1.VolumeMount{{
							Name:      "config",
							MountPath: "/etc/config",
						}},
					}},
					Volumes: []corev1.Volume{{
						Name: "config",
						VolumeSource: corev1.VolumeSource{
							ConfigMap: &corev1.ConfigMapVolumeSource{
								LocalObjectReference: corev1.LocalObjectReference{
									Name: name,
								},
							},
						},
					}},
					InitContainers: initContainers(),
					RestartPolicy:  "Never",
					// FIXME: If this service account is shared btw client and verification, it should have a different name
					ServiceAccountName: "evaluation-client",
				},
			},
		},
	}
}

//+kubebuilder:rbac:groups=dyff.io,resources=evaluations,verbs=get;list;watch;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=evaluations/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=evaluations/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=configmaps;services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps,resources=deployments;deployments/scale,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Evaluation object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *EvaluationReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var logger = log.FromContext(ctx)
	var err error

	var evaluation dyffv1alpha1.Evaluation
	err = r.Get(ctx, req.NamespacedName, &evaluation)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("Evaluation NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get Evaluation")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	// Failed Job => failed Evaluation
	var inferenceFailed bool = false
	var verificationFailed bool = false
	if len(failedJobs) > 0 {
		err := errors.New("failed jobs")
		logger.Error(err, "failed jobs")

		for _, job := range failedJobs {
			if job.ObjectMeta.Labels["dyff.io/step"] == "inference" {
				inferenceFailed = true
			} else if job.ObjectMeta.Labels["dyff.io/step"] == "verification" {
				verificationFailed = true
			}
		}
	}

	var inferenceSucceeded bool = false
	var verificationSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == "inference" {
			inferenceSucceeded = true
		} else if step == "verification" {
			verificationSucceeded = true
		}
	}

	if k8sMeta.IsStatusConditionTrue(evaluation.Status.Conditions, "Complete") ||
		k8sMeta.IsStatusConditionTrue(evaluation.Status.Conditions, "Failed") {
		// Evaluation has reached terminal state

		// Only delete the Jobs after Condition updates are visible, since we
		// use Job status as a cue for which step of the workflow we're in.
		err = r.Delete(ctx, r.createVerificationJob(&evaluation), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
		err = r.Delete(ctx, r.createClientJob(&evaluation), client.PropagationPolicy("Background"))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		return ctrl.Result{}, nil
	}

	// The pattern now is:
	// - If a step ended, set status and clean up resources.
	// - If the step ended because it succeeded, start next step.

	if verificationSucceeded || verificationFailed {
		// If Verification step reached terminal state, set status, clean up,
		// return ok.

		if verificationSucceeded {
			// Update status
			k8sMeta.SetStatusCondition(
				&evaluation.Status.Conditions,
				metav1.Condition{
					Type:               "Complete",
					Status:             metav1.ConditionTrue,
					Reason:             "Complete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Evaluation is complete",
				},
			)
			k8sMeta.SetStatusCondition(
				&evaluation.Status.Conditions,
				metav1.Condition{
					Type:               "Failed",
					Status:             metav1.ConditionFalse,
					Reason:             "Complete",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Evaluation is complete",
				},
			)
			err = r.Status().Update(ctx, &evaluation)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("evaluation workflow succeeded")
		} else if verificationFailed {
			k8sMeta.SetStatusCondition(
				&evaluation.Status.Conditions,
				metav1.Condition{
					Type:               "Failed",
					Status:             metav1.ConditionTrue,
					Reason:             "VerificationFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Verification Job failed",
				},
			)
			// Don't requeue (unless status update fails) -- if we want retry
			// semantics, specify that in the child Jobs
			err = r.Status().Update(ctx, &evaluation)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("evaluation workflow failed")
		}

		// Clean up resources from verification step
		// AFAICT we need a full resource spec to call Delete()
		err = r.Delete(ctx, r.createConfigMap(&evaluation, kinds.EvaluationVerification()))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}
	} else if inferenceSucceeded || inferenceFailed {
		// Else if Inference step reached terminal state, set status, clean up,
		// start verification if inference succeeded.

		if inferenceSucceeded {
			// Update status
			k8sMeta.SetStatusCondition(
				&evaluation.Status.Conditions,
				metav1.Condition{
					Type:               "Complete",
					Status:             metav1.ConditionFalse,
					Reason:             "OutputsNotVerified",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Inference is complete but outputs are not verified",
				},
			)
			err = r.Status().Update(ctx, &evaluation)
			if err != nil {
				return ctrl.Result{}, err
			}
		} else if inferenceFailed {
			k8sMeta.SetStatusCondition(
				&evaluation.Status.Conditions,
				metav1.Condition{
					Type:               "Failed",
					Status:             metav1.ConditionTrue,
					Reason:             "InferenceFailed",
					LastTransitionTime: metav1.NewTime(time.Now()),
					Message:            "Inference Job failed",
				},
			)
			// Don't requeue (unless status update fails) -- if we want retry
			// semantics, specify that in the child Jobs
			err = r.Status().Update(ctx, &evaluation)
			if err != nil {
				return ctrl.Result{}, err
			}
			logger.Info("evaluation workflow failed")
		}

		// Clean up resources from inference job
		if r.hasDependentInferenceSession(&evaluation) {
			err = r.Delete(ctx, r.createInferenceSession(&evaluation))
			if client.IgnoreNotFound(err) != nil {
				return ctrl.Result{}, err
			}
		}
		err = r.Delete(ctx, r.createConfigMap(&evaluation, kinds.EvaluationClient()))
		if client.IgnoreNotFound(err) != nil {
			return ctrl.Result{}, err
		}

		if inferenceSucceeded {
			// Start verification
			config := r.createConfigMap(&evaluation, kinds.EvaluationVerification())
			if err := ctrl.SetControllerReference(&evaluation, config, r.Scheme); err != nil {
				logger.Error(err, "failed to adopt verification ConfigMap")
				return ctrl.Result{}, err
			}
			verificationJob := r.createVerificationJob(&evaluation)
			if err := ctrl.SetControllerReference(&evaluation, verificationJob, r.Scheme); err != nil {
				logger.Error(err, "failed to adopt verification Job")
				return ctrl.Result{}, err
			}
			// Create the resources. Ignore AlreadyExists errors in case some
			// Create operations succeeded in previous Reconcile calls
			err = r.Create(ctx, config)
			if client.IgnoreAlreadyExists(err) != nil {
				logger.Error(err, "failed to create verification ConfigMap", "config", config)
				return ctrl.Result{}, err
			}
			err = r.Create(ctx, verificationJob)
			if client.IgnoreAlreadyExists(err) != nil {
				logger.Error(err, "failed to create verification Job", "verificationJob", verificationJob)
				return ctrl.Result{}, err
			}

			logger.Info("created workflow evaluation.verification")
		}
	} else {
		// Else no jobs have run, so start inference, set status.

		// Start inference
		config := r.createConfigMap(&evaluation, kinds.EvaluationClient())
		if err := ctrl.SetControllerReference(&evaluation, config, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt client ConfigMap")
			return ctrl.Result{}, err
		}
		clientJob := r.createClientJob(&evaluation)
		if err := ctrl.SetControllerReference(&evaluation, clientJob, r.Scheme); err != nil {
			logger.Error(err, "failed to adopt client Job")
			return ctrl.Result{}, err
		}
		var session *dyffv1alpha1.InferenceSession
		if r.hasDependentInferenceSession(&evaluation) {
			session = r.createInferenceSession(&evaluation)
			if err := ctrl.SetControllerReference(&evaluation, session, r.Scheme); err != nil {
				logger.Error(err, "failed to adopt inference Session")
				return ctrl.Result{}, err
			}
		}
		// Create the resources. Ignore AlreadyExists errors in case some
		// Create operations succeeded in previous Reconcile calls
		err = r.Create(ctx, config)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create client ConfigMap", "config", config)
			return ctrl.Result{}, err
		}
		err = r.Create(ctx, clientJob)
		if client.IgnoreAlreadyExists(err) != nil {
			logger.Error(err, "failed to create client Job", "clientJob", clientJob)
			return ctrl.Result{}, err
		}
		if r.hasDependentInferenceSession(&evaluation) {
			err = r.Create(ctx, session)
			if client.IgnoreAlreadyExists(err) != nil {
				logger.Error(err, "failed to create inference Session", "session", session)
				return ctrl.Result{}, err
			}
		}

		logger.Info("created workflow evaluation.inference")

		k8sMeta.SetStatusCondition(
			&evaluation.Status.Conditions,
			metav1.Condition{
				Type:               "Complete",
				Status:             metav1.ConditionFalse,
				Reason:             "InferenceIncomplete",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Inference is running",
			},
		)
		k8sMeta.SetStatusCondition(
			&evaluation.Status.Conditions,
			metav1.Condition{
				Type:               "Failed",
				Status:             metav1.ConditionFalse,
				Reason:             "Running",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "Evaluation is running normally",
			},
		)
		if err := r.Status().Update(ctx, &evaluation); err != nil {
			return ctrl.Result{}, err
		}
	}

	// Return OK
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *EvaluationReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.Evaluation)

	// Cache Jobs whose owner is an Evaluation
	err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.Evaluation {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": EvaluationWorkflowPlural(),
		},
	})
	if err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.Evaluation{}).
		Owns(&dyffv1alpha1.InferenceSession{}, builder.WithPredicates(workflowPredicate)).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.ConfigMap{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
