// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ReportSpec defines the desired state of Report
type ReportSpec struct {
	DyffEntity `json:",inline"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Report string `json:"report"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Dataset EntityID `json:"dataset"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	DatasetAdapter []SchemaAdapter `json:"datasetAdapter,omitempty"`

	// +kubebuilder:validation:Required
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Evaluation EntityID `json:"evaluation"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	EvaluationAdapter []SchemaAdapter `json:"evaluationAdapter,omitempty"`

	// +kubebuilder:default={}
	// +operator-sdk:csv:customresourcedefinitions:type=spec
	Modules []EntityID `json:"modules,omitempty"`
}

// ReportStatus defines the observed state of Report
type ReportStatus struct {
	// +operator-sdk:csv:customresourcedefinitions:type=status
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Report is the Schema for the reports API
type Report struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ReportSpec   `json:"spec,omitempty"`
	Status ReportStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ReportList contains a list of Report
type ReportList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Report `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Report{}, &ReportList{})
}
