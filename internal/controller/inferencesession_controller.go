// SPDX-FileCopyrightText: 2024 UL Research Institutes
// SPDX-License-Identifier: Apache-2.0

package controller

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"golang.org/x/exp/slices"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	k8sMeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	dyffv1alpha1 "gitlab.com/dyff/dyff-operator/api/v1alpha1"
	"gitlab.com/dyff/dyff-operator/internal/kinds"
)

// InferenceSessionReconciler reconciles a InferenceSession object
type InferenceSessionReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	ownerKey string
}

type InferenceSessionReconcilerKernel struct {
	DyffReconcilerKernel

	InferenceSession *dyffv1alpha1.InferenceSession
	reconciler       *InferenceSessionReconciler
	ctx              context.Context
	logger           logr.Logger
}

func (r *InferenceSessionReconciler) newKernel(ctx context.Context, session *dyffv1alpha1.InferenceSession) *InferenceSessionReconcilerKernel {
	return &InferenceSessionReconcilerKernel{
		DyffReconcilerKernel: DyffReconcilerKernel{
			Workflow:             InferenceSessionWorkflowPlural(),
			Kind:                 kinds.InferenceSession,
			ShortName:            kinds.InferenceSessionShort,
			WorkflowSingleLetter: InferenceSessionWorkflowSingleLetter(),
		},
		InferenceSession: session,
		reconciler:       r,
		ctx:              ctx,
		logger:           log.FromContext(ctx),
	}
}

func InferenceSessionWorkflowPlural() string {
	return "inferencesessions"
}

func InferenceSessionWorkflowSingleLetter() string {
	return "i"
}

func InferenceSessionStatusDownloadSuccessful() string {
	return fmt.Sprintf("%v.dyff.io/DownloadSuccessful", InferenceSessionWorkflowPlural())
}

// Derive labels for an InferenceSession component from the InferenceSession
// labels. In particular, set the "dyff.io/workflow" label to
// "inferencesessions" so that this controller watches the component.
func (k *InferenceSessionReconcilerKernel) derivedLabels() map[string]string {
	labels := make(map[string]string)
	for k, v := range k.InferenceSession.Labels {
		labels[k] = v
	}
	labels["dyff.io/workflow"] = InferenceSessionWorkflowPlural()
	return labels
}

// Derive labels for an InferenceSession component from the InferenceSession
// labels. In particular, set the "dyff.io/workflow" label to
// "inferencesessions" so that this controller watches the component, and add
// the "dyff.io/step" label for progress tracking.
func (k *InferenceSessionReconcilerKernel) derivedLabelsForStep(step kinds.WorkflowComponent) map[string]string {
	labels := k.derivedLabels()
	labels["dyff.io/step"] = step.Component
	return labels
}

func (k *InferenceSessionReconcilerKernel) getArtifactsPersistentVolumeClaimName() string {
	return fmt.Sprintf("%va", k.InferenceSession.Name)
}

func (k *InferenceSessionReconcilerKernel) getDownloadJobName() string {
	return fmt.Sprintf("%vd", k.InferenceSession.Name)
}

func (k *InferenceSessionReconcilerKernel) getInferenceStepName() string {
	return fmt.Sprintf("%vi", k.InferenceSession.Name)
}

func (k *InferenceSessionReconcilerKernel) getArtifactsPersistentVolumeClaimManifest() *corev1.PersistentVolumeClaim {
	if k.needsDownloadStep() {
		// Inherit metadata from InferenceSession so that we have a UID for
		// dependent sessions
		return k.DyffReconcilerKernel.getArtifactsPersistentVolumeClaimManifest(
			k.InferenceSession.Labels,
			k.getArtifactsPersistentVolumeClaimName(),
			k.InferenceSession.Namespace,
			k.InferenceSession.Spec.ArtifactsVolume.Storage,
		)
	} else {
		return nil
	}
}

// Create the Service resource for the InferenceService Deployment
func (k *InferenceSessionReconcilerKernel) getServiceManifest() *corev1.Service {
	selector := k.derivedLabelsForStep(kinds.InferenceSessionInference())

	// In multi-node config, route all requests to the "leader" pod (pod-0) only
	// because "worker" pods aren't running an inference API server
	if k.InferenceSession.Spec.Nodes > 1 {
		selector["statefulset.kubernetes.io/pod-name"] = fmt.Sprintf("%v-0", k.getInferenceStepName())
	}

	return &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			// Inherit metadata from InferenceSession so that we have a UID for
			// dependent sessions
			Labels:    k.derivedLabels(),
			Name:      k.getInferenceStepName(),
			Namespace: k.InferenceSession.Namespace,
		},
		Spec: corev1.ServiceSpec{
			// This must match the labels for the Deployment
			Selector: selector,
			Type:     corev1.ServiceType("ClusterIP"),
			Ports: []corev1.ServicePort{
				{
					Protocol:   "TCP",
					Port:       80,
					TargetPort: intstr.FromInt(8000),
				},
			},
		},
	}
}

func (k *InferenceSessionReconcilerKernel) needsDownloadStep() bool {
	return k.InferenceSession.Spec.ArtifactsVolume != nil
}

func (k *InferenceSessionReconcilerKernel) getArtifactsDownloadJobManifest() *batchv1.Job {
	if !k.needsDownloadStep() {
		return nil
	} else {
		return k.DyffReconcilerKernel.getArtifactsDownloadJobManifest(
			k.InferenceSession.Spec.ArtifactsVolume.Artifacts,
			k.derivedLabelsForStep(k.ArtifactsDownload()),
			k.getDownloadJobName(),
			k.InferenceSession.Namespace,
			k.getArtifactsPersistentVolumeClaimName(),
		)
	}
}

// ResourceProfile stores the available compute resources that can be requested.
type ResourceProfile struct {
	CpuCount            int64
	MemoryGiB           int64
	EphemeralStorageGiB int64
	GpuType             string
	GpuCount            int64
	NodeSelector        map[string]string
}

// TODO: (DYFF-606) Should get this info from configuration
// https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-resource-requests#hardware-min-max
// We subtract 2 vCPUs and 14Gi memory from the maximums to accomodate the
// maximum resource requests for DaemonSets.
// Most Accelerator nodes are limited to 122Gi of ephemeral storage unless
// we attach additional disks.
var gcpNodeProfiles = [...]ResourceProfile{
	// CPU only
	// TODO: Add more options, maybe allow users to specify CPUs and/or memory
	{
		// This is a guess at a valid Autopilot config for a c3-standard-22 machine
		// (based on other configs, we have to reserve some VM resources for Autopilot stuff)
		// Worst case, we overshoot and get a c3-standard-44 instead.
		CpuCount:            20,
		MemoryGiB:           78,
		EphemeralStorageGiB: 250, // This value is officially documented
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class":  "Performance",
			"cloud.google.com/machine-family": "c3",
		},
	},
	// Nvidia A100 GPUs
	{
		CpuCount:            9,
		MemoryGiB:           60,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-a100",
		GpuCount:            1,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            20,
		MemoryGiB:           134,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-a100",
		GpuCount:            2,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            44,
		MemoryGiB:           296,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-a100",
		GpuCount:            4,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            92,
		MemoryGiB:           618,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-a100",
		GpuCount:            8,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            92,
		MemoryGiB:           1250,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-a100",
		GpuCount:            16,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	// Nvidia H100 GPUs
	{
		CpuCount:            206,
		MemoryGiB:           1795,
		EphemeralStorageGiB: 5250,
		GpuType:             "nvidia.com/gpu-h100",
		GpuCount:            8,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	// Nvidia L4 GPUs
	{
		CpuCount:            29,
		MemoryGiB:           101,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-l4",
		GpuCount:            1,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            21,
		MemoryGiB:           69,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-l4",
		GpuCount:            2,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            45,
		MemoryGiB:           163,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-l4",
		GpuCount:            4,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            93,
		MemoryGiB:           349,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-l4",
		GpuCount:            8,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	// Nvidia T4 GPUs
	{
		CpuCount:            44,
		MemoryGiB:           273,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-t4",
		GpuCount:            1,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            44,
		MemoryGiB:           273,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-t4",
		GpuCount:            2,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
	{
		CpuCount:            92,
		MemoryGiB:           573,
		EphemeralStorageGiB: 122,
		GpuType:             "nvidia.com/gpu-t4",
		GpuCount:            4,
		NodeSelector: map[string]string{
			"cloud.google.com/compute-class": "Accelerator",
		},
	},
}

// Subtract the resources in the argument from this ResourceProfile.
// Panics if the result would have negative resource amounts or the operation
// doesn't make sense for some other reason (e.g., different GpuTypes).
func (p *ResourceProfile) Subtract(x ResourceProfile) ResourceProfile {
	if x.GpuCount > 0 && p.GpuType != x.GpuType {
		panic("can't subtract profile with different GpuType")
	}
	diff := ResourceProfile{
		CpuCount:            p.CpuCount - x.CpuCount,
		MemoryGiB:           p.MemoryGiB - x.MemoryGiB,
		EphemeralStorageGiB: p.EphemeralStorageGiB - x.EphemeralStorageGiB,
		GpuType:             p.GpuType,
		GpuCount:            p.GpuCount - x.GpuCount,
	}
	if diff.CpuCount < 0 || diff.MemoryGiB < 0 || diff.EphemeralStorageGiB < 0 || diff.GpuCount < 0 {
		panic("difference results in negative resources")
	}
	return diff
}

// Create a k8s resource request spec for the resources in this profile.
func (p *ResourceProfile) ResourceRequests() corev1.ResourceList {
	rs := corev1.ResourceList{
		"cpu": *resource.NewQuantity(p.CpuCount, resource.DecimalSI),
		// We currently don't need ephemeral-storage for the model
		// "ephemeral-storage": resource.MustParse(fmt.Sprintf("%vGi", p.EphemeralStorageGiB)),
		"memory": resource.MustParse(fmt.Sprintf("%vGi", p.MemoryGiB)),
	}
	if p.GpuCount > 0 {
		rs["nvidia.com/gpu"] = *resource.NewQuantity(p.GpuCount, resource.DecimalSI)
	}
	return rs
}

// Create a k8s resource limit spec for the resources in this profile.
func (p *ResourceProfile) ResourceLimits() corev1.ResourceList {
	rs := corev1.ResourceList{}
	if p.GpuCount > 0 {
		rs["nvidia.com/gpu"] = *resource.NewQuantity(p.GpuCount, resource.DecimalSI)
	}
	return rs
}

// Create a ResourceProfile that is compatible with the given InferenceSession spec.
// Returns an error if none of the node types on the current platform can
// accomodate the resource request.
func NewResourceProfile(session *dyffv1alpha1.InferenceSession) (*ResourceProfile, error) {
	if session.Spec.Accelerator == nil {
		// CPU node
		for _, profile := range gcpNodeProfiles {
			if profile.GpuCount == 0 {
				return &profile, nil
			}
		}
	} else {
		if session.Spec.Accelerator.Kind != "GPU" || session.Spec.Accelerator.Gpu == nil {
			return nil, errors.New("only 'GPU' is currently supported for .accelerator")
		}

		for _, profile := range gcpNodeProfiles {
			if profile.GpuCount == int64(session.Spec.Accelerator.Gpu.Count) &&
				slices.Contains(session.Spec.Accelerator.Gpu.HardwareTypes, profile.GpuType) {
				return &profile, nil
			}
		}
	}
	return nil, errors.New("no matching ResourceProfile for session configuration")
}

// FUSEConfig stores the resource request/limit configuration
// for the FUSE CSI sidecar container.
type FUSEConfig struct {
	CPURequest              resource.Quantity
	EphemeralStorageRequest resource.Quantity
	MemoryRequest           resource.Quantity

	CPULimit              resource.Quantity
	EphemeralStorageLimit resource.Quantity
	MemoryLimit           resource.Quantity

	FileCacheMaxSizeMB int64
}

// Create a FUSEConfig appropriate for the given ResourceProfile.
// Returns the FUSEConfig, and a new ResourceProfile containing the
// remaining resources that are not allocated to the FUSE CSI driver.
func MakeFUSEConfig(profile ResourceProfile) (FUSEConfig, ResourceProfile) {
	const spread int64 = 2
	var units int64 = 1
	if profile.GpuCount > 0 {
		units = int64(profile.GpuCount)
	}

	// TODO: (DYFF-606) Choose from known-good configurations instead of
	// assuming these defaults will work
	fuseProfile := ResourceProfile{
		CpuCount:            2 * units,
		MemoryGiB:           8 + (8 * units),
		EphemeralStorageGiB: 8 + (8 * units),
	}

	fuseConfig := FUSEConfig{
		CPURequest:              *resource.NewQuantity(fuseProfile.CpuCount, resource.DecimalSI),
		CPULimit:                *resource.NewQuantity(fuseProfile.CpuCount, resource.DecimalSI),
		EphemeralStorageRequest: resource.MustParse(fmt.Sprintf("%vGi", fuseProfile.EphemeralStorageGiB)),
		EphemeralStorageLimit:   resource.MustParse(fmt.Sprintf("%vGi", fuseProfile.EphemeralStorageGiB)),
		MemoryRequest:           resource.MustParse(fmt.Sprintf("%vGi", fuseProfile.MemoryGiB)),
		MemoryLimit:             resource.MustParse(fmt.Sprintf("%vGi", fuseProfile.MemoryGiB)),
		FileCacheMaxSizeMB:      (fuseProfile.EphemeralStorageGiB / 2) * 1024,
	}
	newResourceProfile := profile.Subtract(fuseProfile)
	return fuseConfig, newResourceProfile
}

// Add annotations corresponnding to the resource requests/limits in this
// FUSEConfig to the provided map.
func (c *FUSEConfig) Annotate(annotations map[string]string) {
	// TODO: This FUSE CSI config is gcloud-specific
	// https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/cloud-storage-fuse-csi-driver
	// https://github.com/GoogleCloudPlatform/gcs-fuse-csi-driver/issues/153
	// https://cloud.google.com/kubernetes-engine/docs/how-to/cloud-storage-fuse-csi-driver-sidecar#configure-sidecar-resources
	annotations["gke-gcsfuse/volumes"] = "true"
	annotations["gke-gcsfuse/cpu-request"] = c.CPURequest.String()
	annotations["gke-gcsfuse/cpu-limit"] = c.CPULimit.String()
	annotations["gke-gcsfuse/ephemeral-storage-request"] = c.EphemeralStorageRequest.String()
	annotations["gke-gcsfuse/ephemeral-storage-limit"] = c.EphemeralStorageLimit.String()
	annotations["gke-gcsfuse/memory-request"] = c.MemoryRequest.String()
	annotations["gke-gcsfuse/memory-limit"] = c.MemoryLimit.String()
}

// Create a CSIVolumeSource resource that mounts the given path in the given
// bucket with appropriate configuration options.
func (c *FUSEConfig) CSIVolumeSource(bucketName string, bucketPath string) *corev1.CSIVolumeSource {
	return &corev1.CSIVolumeSource{
		Driver:   "gcsfuse.csi.storage.gke.io",
		ReadOnly: Pointer(true),
		VolumeAttributes: map[string]string{
			"bucketName": bucketName,
			// Mount only the artifact sub-directory, for security
			// Non-root group/user must be set:
			// https://github.com/GoogleCloudPlatform/gcs-fuse-csi-driver/blob/main/docs/troubleshooting.md#io-errors-in-your-workloads
			"mountOptions": fmt.Sprintf(
				"implicit-dirs,only-dir=%v,gid=%v,uid=%v,file-cache:max-size-mb:%v,file-cache:enable-parallel-downloads:true",
				bucketPath,
				ORDINARY_GROUP,
				ORDINARY_USER,
				c.FileCacheMaxSizeMB,
			),
		},
	}
}

func (k *InferenceSessionReconcilerKernel) getPlatformGpuType(profile ResourceProfile) *string {
	if profile.GpuType == "nvidia.com/gpu-a100" {
		return Pointer("nvidia-tesla-a100")
	} else if profile.GpuType == "nvidia.com/gpu-a100-80gb" {
		return Pointer("nvidia-a100-80gb")
	} else if profile.GpuType == "nvidia.com/gpu-h100" {
		return Pointer("nvidia-h100-80gb")
	} else if profile.GpuType == "nvidia.com/gpu-h200" {
		return Pointer("nvidia-h200-141gb")
	} else if profile.GpuType == "nvidia.com/gpu-l4" {
		return Pointer("nvidia-l4")
	} else if profile.GpuType == "nvidia.com/gpu-t4" {
		return Pointer("nvidia-tesla-t4")
	} else {
		return nil
	}
}

func (k *InferenceSessionReconcilerKernel) getInferencePodTemplateSpec() (*corev1.PodTemplateSpec, error) {
	// Configurable Deployment properties
	const tmpName string = "tmp"
	const tmpPath string = "/tmp"
	var volumeMounts []corev1.VolumeMount
	var volumes []corev1.Volume
	var terminationGracePeriodSeconds *int64 = nil
	var nodeSelectorMatchExpressions []corev1.NodeSelectorRequirement
	var annotations map[string]string = make(map[string]string)

	resourceProfilePtr, err := NewResourceProfile(k.InferenceSession)
	if err != nil {
		return nil, err
	}
	var resourceProfile = *resourceProfilePtr
	var fuseConfig FUSEConfig

	var tmpSize resource.Quantity = resource.MustParse("100Mi")
	// FIXME: Gpu.Memory is deprecated; need a modelSize field
	if k.InferenceSession.Spec.Accelerator != nil &&
		k.InferenceSession.Spec.Accelerator.Gpu != nil &&
		k.InferenceSession.Spec.Accelerator.Gpu.Memory != nil {
		// This is probably much larger than needed, but I don't know what
		// the runner actually uses /tmp for
		tmpSize = *k.InferenceSession.Spec.Accelerator.Gpu.Memory
	}

	// Mount writable volume at /tmp
	// TODO: (DYFF-606) This should be an SSD if possible
	volumes = append(volumes, corev1.Volume{
		Name: tmpName,
		VolumeSource: corev1.VolumeSource{
			Ephemeral: &corev1.EphemeralVolumeSource{
				VolumeClaimTemplate: &corev1.PersistentVolumeClaimTemplate{
					Spec: corev1.PersistentVolumeClaimSpec{
						AccessModes: []corev1.PersistentVolumeAccessMode{"ReadWriteOnce"},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								"storage": tmpSize,
							},
						},
						// TODO: GKE-specific name
						// StorageClassName: Pointer("standard-rwo"),
					},
				},
			},
		},
	})
	volumeMounts = append(volumeMounts, corev1.VolumeMount{
		Name:      tmpName,
		MountPath: tmpPath,
	})

	// Mount artifacts, if present
	if k.InferenceSession.Spec.ArtifactsVolume != nil {
		av := k.InferenceSession.Spec.ArtifactsVolume
		volumeMounts = append(volumeMounts, corev1.VolumeMount{
			Name:      av.Name,
			MountPath: av.MountPath,
		})
		volumes = append(volumes, corev1.Volume{
			Name: av.Name,
			VolumeSource: corev1.VolumeSource{
				PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
					ClaimName: k.getArtifactsPersistentVolumeClaimName(),
					ReadOnly:  true,
				},
			},
		})
	}

	// Handle dependencies
	for _, dependency := range k.InferenceSession.Spec.Dependencies {
		if dependency.Kind == "EphemeralVolume" {
			if dependency.EphemeralVolume == nil {
				return nil, errors.New("ephemeralVolume is required")
			}
			volumeMounts = append(volumeMounts, corev1.VolumeMount{
				Name:      dependency.EphemeralVolume.Name,
				MountPath: dependency.EphemeralVolume.MountPath,
			})
			volumes = append(volumes, corev1.Volume{
				Name: dependency.EphemeralVolume.Name,
				VolumeSource: corev1.VolumeSource{
					Ephemeral: &corev1.EphemeralVolumeSource{
						VolumeClaimTemplate: &corev1.PersistentVolumeClaimTemplate{
							Spec: corev1.PersistentVolumeClaimSpec{
								AccessModes: []corev1.PersistentVolumeAccessMode{
									"ReadWriteOnce",
								},
								Resources: corev1.ResourceRequirements{
									Requests: corev1.ResourceList{
										"storage": dependency.EphemeralVolume.Storage,
									},
								},
							},
						},
					},
				},
			})
		} else if dependency.Kind == "FUSEVolume" {
			if dependency.FUSEVolume == nil {
				return nil, errors.New("fuseVolume is required")
			}
			storageUrl, err := url.Parse(artifactStorageUrl(
				dependency.FUSEVolume.Artifact.Kind,
				dependency.FUSEVolume.Artifact.Id,
			))
			if err != nil {
				return nil, err
			}
			bucketName := storageUrl.Hostname()
			if bucketName == "" {
				return nil, fmt.Errorf("no bucket name in URL: %v", storageUrl)
			}
			bucketPath := strings.Trim(storageUrl.Path, "/")
			if bucketPath == "" {
				return nil, fmt.Errorf("no artifact path in URL: %v", storageUrl)
			}

			fuseConfig, resourceProfile = MakeFUSEConfig(resourceProfile)
			fuseConfig.Annotate(annotations)

			volumeMounts = append(volumeMounts, corev1.VolumeMount{
				Name:      dependency.FUSEVolume.Name,
				MountPath: dependency.FUSEVolume.MountPath,
				ReadOnly:  true,
			})
			volumes = append(volumes, corev1.Volume{
				Name: dependency.FUSEVolume.Name,
				VolumeSource: corev1.VolumeSource{
					// https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/cloud-storage-fuse-csi-driver#deploy-pod
					CSI: fuseConfig.CSIVolumeSource(bucketName, bucketPath),
				},
			})
		} else if dependency.Kind == "ReadOnlyVolume" {
			if dependency.ReadOnlyVolume == nil {
				return nil, errors.New("readOnlyVolume is required")
			}
			volumeMounts = append(volumeMounts, corev1.VolumeMount{
				Name:      dependency.ReadOnlyVolume.Name,
				MountPath: dependency.ReadOnlyVolume.MountPath,
				ReadOnly:  true,
			})
			volumes = append(volumes, corev1.Volume{
				Name: dependency.ReadOnlyVolume.Name,
				VolumeSource: corev1.VolumeSource{
					PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
						ClaimName: dependency.ReadOnlyVolume.ClaimName,
						ReadOnly:  true,
					},
				},
			})
		} else {
			return nil, fmt.Errorf("unknown dependency kind %v", dependency.Kind)
		}
	}

	// At this point, resourceProfile has been adjusted to account for resource
	// requests of dependencies. Everything that's left is available for the
	// main model Pods.

	// Node selector requirements
	if resourceProfile.GpuCount > 0 {
		gcloudGpuType := k.getPlatformGpuType(resourceProfile)
		if gcloudGpuType == nil {
			return nil, fmt.Errorf("no supported hardware type given: %v", resourceProfile.GpuType)
		}
		// TODO: How to provision GPUs is platform-specific; this is how you
		// do it on GKE Autopilot: https://cloud.google.com/kubernetes-engine/docs/how-to/autopilot-gpus#request-gpus

		// TODO: Translate platform-agnostic hardware type to platform-specific
		nodeSelectorMatchExpressions = append(
			nodeSelectorMatchExpressions,
			corev1.NodeSelectorRequirement{
				Key:      "cloud.google.com/gke-accelerator",
				Operator: "In",
				Values:   []string{*gcloudGpuType},
			},
		)

		if k.InferenceSession.Spec.Accelerator.Gpu.Count > 1 {
			// vLLM / ray recommend /dev/shm allocation of >30% of total memory
			// TODO: This probably doesn't apply to non-vLLM engines; check which
			// engine is in use.
			memoryQuantity := resource.MustParse(fmt.Sprintf("%vGi", resourceProfile.MemoryGiB))
			memoryBytes, ok := memoryQuantity.AsInt64()
			if !ok {
				return nil, fmt.Errorf("failed to convert memory request to int64: %v", memoryQuantity)
			}
			shmBytes := (memoryBytes / 100) * 30
			shmQuantity := resource.NewQuantity(shmBytes, resource.DecimalSI)
			shmQuantity.RoundUp(resource.Mega)

			volumeMounts = append(volumeMounts, corev1.VolumeMount{
				Name:      "shm",
				MountPath: "/dev/shm",
			})
			volumes = append(volumes, corev1.Volume{
				Name: "shm",
				VolumeSource: corev1.VolumeSource{
					EmptyDir: &corev1.EmptyDirVolumeSource{
						Medium:    corev1.StorageMediumMemory,
						SizeLimit: shmQuantity,
					},
				},
			})
		}
	}

	if k.InferenceSession.Spec.UseSpotPods {
		// TODO: (GKE-specific) This will require a different selector on different clouds
		nodeSelectorMatchExpressions = append(
			nodeSelectorMatchExpressions,
			corev1.NodeSelectorRequirement{
				Key:      "cloud.google.com/gke-spot",
				Operator: "In",
				Values:   []string{"true"},
			},
		)
		// (GKE-specific) GKE recommended value (?)
		var i int64 = 25
		terminationGracePeriodSeconds = &i
	}

	var affinity *corev1.Affinity = nil
	if len(nodeSelectorMatchExpressions) > 0 {
		affinity = &corev1.Affinity{
			NodeAffinity: &corev1.NodeAffinity{
				RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
					NodeSelectorTerms: []corev1.NodeSelectorTerm{
						{MatchExpressions: nodeSelectorMatchExpressions},
					},
				},
			},
		}
	}

	// FIXME: (GKE-specific) Accelerator nodes are billed per-node, so there
	// seems to be no reason not to request all available resources.
	var resourceRequirements = corev1.ResourceRequirements{
		Limits:   resourceProfile.ResourceLimits(),
		Requests: resourceProfile.ResourceRequests(),
	}

	var podTemplateSpec = &corev1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			// This must match the labels for the Service
			Labels:      k.derivedLabelsForStep(kinds.InferenceSessionInference()),
			Annotations: annotations,
		},
		Spec: corev1.PodSpec{
			Affinity: affinity,
			Containers: []corev1.Container{{
				Name:    k.getInferenceStepName(),
				Image:   k.InferenceSession.Spec.Image,
				Env:     k.InferenceSession.Spec.Env,
				Command: k.InferenceSession.Spec.Command,
				Args:    k.InferenceSession.Spec.Args,
				Ports: []corev1.ContainerPort{
					{ContainerPort: 8000},
				},
				Resources:       resourceRequirements,
				SecurityContext: workflowContainerSecurityContext(),
				VolumeMounts:    volumeMounts,
			}},
			NodeSelector:    resourceProfile.NodeSelector,
			SecurityContext: workflowsPodSecurityContext(),
			// TODO: Make configurable
			ServiceAccountName:            "inferencesession-runner",
			Volumes:                       volumes,
			TerminationGracePeriodSeconds: terminationGracePeriodSeconds,
		},
	}

	return podTemplateSpec, nil
}

func (k *InferenceSessionReconcilerKernel) getSingleNodeDeploymentManifest() (*appsv1.Deployment, error) {
	if k.InferenceSession.Spec.Nodes > 1 {
		panic("Multi-node configurations should not be using this function")
	}

	podTemplateSpec, err := k.getInferencePodTemplateSpec()
	if err != nil {
		return nil, err
	}

	var deployment *appsv1.Deployment = &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    k.derivedLabelsForStep(kinds.InferenceSessionInference()),
			Name:      k.getInferenceStepName(),
			Namespace: k.InferenceSession.Namespace,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &k.InferenceSession.Spec.Replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: k.derivedLabelsForStep(kinds.InferenceSessionInference()),
			},
			Template: *podTemplateSpec,
		},
	}

	return deployment, nil
}

func (k *InferenceSessionReconcilerKernel) getMultiNodeHeadlessServiceName() string {
	return fmt.Sprintf("%v-nodes", k.getInferenceStepName())
}

func (k *InferenceSessionReconcilerKernel) getMultiNodeStatefulSetManifest() (*appsv1.StatefulSet, error) {
	if k.InferenceSession.Spec.Nodes <= 1 {
		panic("Single-node configurations should not be using this function")
	}

	podTemplateSpec, err := k.getInferencePodTemplateSpec()
	if err != nil {
		return nil, err
	}

	// This is a multi-node deployment
	env := podTemplateSpec.Spec.Containers[0].Env
	env = append(env, corev1.EnvVar{
		Name:  "DYFF_INFERENCESESSIONS__MULTI_NODE",
		Value: "1",
	})
	// Pods need to know their index, so that pod-0 can be the "leader"
	env = append(env, corev1.EnvVar{
		Name: "DYFF_INFERENCESESSIONS__POD_INDEX",
		ValueFrom: &corev1.EnvVarSource{
			FieldRef: &corev1.ObjectFieldSelector{
				// https://kubernetes.io/docs/reference/labels-annotations-taints/#apps-kubernetes.io-pod-index
				FieldPath: "metadata.labels['apps.kubernetes.io/pod-index']",
			},
		},
	})
	// Worker pods need to know how to contact the leader (pod-0)
	// The address is a subdomain of the *headless* service.
	leaderHost := fmt.Sprintf(
		"%v-0.%v.%v.svc.cluster.local",
		k.getInferenceStepName(),
		k.getMultiNodeHeadlessServiceName(),
		k.InferenceSession.Namespace,
	)
	env = append(env, corev1.EnvVar{
		Name:  "DYFF_INFERENCESESSIONS__LEADER_HOST",
		Value: leaderHost,
	})
	podTemplateSpec.Spec.Containers[0].Env = env

	var statefulset *appsv1.StatefulSet = &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    k.derivedLabelsForStep(kinds.InferenceSessionInference()),
			Name:      k.getInferenceStepName(),
			Namespace: k.InferenceSession.Namespace,
		},
		Spec: appsv1.StatefulSetSpec{
			ServiceName: k.getMultiNodeHeadlessServiceName(),
			// Replicas in the StatefulSet correspond to nodes in a multi-node setup
			Replicas: &k.InferenceSession.Spec.Nodes,
			Selector: &metav1.LabelSelector{
				MatchLabels: k.derivedLabelsForStep(kinds.InferenceSessionInference()),
			},
			Template: *podTemplateSpec,
			// Start the pods concurrently
			PodManagementPolicy: "Parallel",
		},
	}

	return statefulset, nil
}

func (k *InferenceSessionReconcilerKernel) getMultiNodeHeadlessServiceManifest() (*corev1.Service, error) {
	if k.InferenceSession.Spec.Nodes <= 1 {
		panic("Single-node configurations should not be using this function")
	}

	var service *corev1.Service = &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    k.derivedLabelsForStep(kinds.InferenceSessionInference()),
			Name:      k.getMultiNodeHeadlessServiceName(),
			Namespace: k.InferenceSession.Namespace,
		},
		Spec: corev1.ServiceSpec{
			ClusterIP: "None", // Headless service
			Selector:  k.derivedLabelsForStep(kinds.InferenceSessionInference()),
		},
	}

	return service, nil
}

func (k *InferenceSessionReconcilerKernel) onDownloadSucceeded() (result ctrl.Result, err error) {
	k8sMeta.SetStatusCondition(
		&k.InferenceSession.Status.Conditions,
		metav1.Condition{
			Type:               InferenceSessionStatusDownloadSuccessful(),
			Status:             metav1.ConditionTrue,
			Reason:             "DownloadStepComplete",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Download step is Complete",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.InferenceSession)
	if err != nil {
		return ctrl.Result{}, err
	}

	return k.createInferenceSession()
}

func (k *InferenceSessionReconcilerKernel) onDownloadFailed() (result ctrl.Result, err error) {
	k8sMeta.SetStatusCondition(
		&k.InferenceSession.Status.Conditions,
		metav1.Condition{
			Type:               InferenceSessionStatusDownloadSuccessful(),
			Status:             metav1.ConditionFalse,
			Reason:             "DownloadStepFailed",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "The Download step Failed",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.InferenceSession)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (k *InferenceSessionReconcilerKernel) onCreate() (result ctrl.Result, err error) {
	if k.needsDownloadStep() {
		result, err = k.createDownloadJob()
		if err != nil {
			return result, err
		}
	} else {
		result, err = k.createInferenceSession()
		if err != nil {
			return result, err
		}
	}

	k8sMeta.SetStatusCondition(
		&k.InferenceSession.Status.Conditions,
		metav1.Condition{
			Type:               StatusError,
			Status:             metav1.ConditionFalse,
			Reason:             "Running",
			LastTransitionTime: metav1.NewTime(time.Now()),
			Message:            "InferenceSession workflow is running.",
		},
	)
	err = k.reconciler.Status().Update(k.ctx, k.InferenceSession)
	if err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (k *InferenceSessionReconcilerKernel) createDownloadJob() (result ctrl.Result, err error) {
	// Create PVC for the artifacts
	pvc := k.getArtifactsPersistentVolumeClaimManifest()
	if err := ctrl.SetControllerReference(k.InferenceSession, pvc, k.reconciler.Scheme); err != nil {
		k.logger.Error(err, "failed to adopt artifacts PersistenVolumeClaim")
		return ctrl.Result{}, err
	}
	err = k.reconciler.Create(k.ctx, pvc)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create artifacts PersistentVolumeClaim", "storage", pvc)
		return ctrl.Result{}, err
	}

	downloadJob := k.getArtifactsDownloadJobManifest()
	if err := ctrl.SetControllerReference(k.InferenceSession, downloadJob, k.reconciler.Scheme); err != nil {
		k.logger.Error(err, "failed to adopt download Job")
		return ctrl.Result{}, err
	}
	err = k.reconciler.Create(k.ctx, downloadJob)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create download Job", "downloadJob", downloadJob)
		return ctrl.Result{}, err
	}

	k.logger.Info("inferencesession workflow step: Download")

	return ctrl.Result{}, nil
}

func (k *InferenceSessionReconcilerKernel) createInferenceSession() (result ctrl.Result, err error) {
	service := k.getServiceManifest()
	if err := ctrl.SetControllerReference(k.InferenceSession, service, k.reconciler.Scheme); err != nil {
		k.logger.Error(err, "failed to adopt inference Service")
		return ctrl.Result{}, err
	}
	err = k.reconciler.Create(k.ctx, service)
	if client.IgnoreAlreadyExists(err) != nil {
		k.logger.Error(err, "failed to create inference Service", "service", service)
		return ctrl.Result{}, err
	}

	if k.InferenceSession.Spec.Nodes > 1 {
		// Multi-node configuration: StatefulSet + additional "headless" Service

		statefulset, err := k.getMultiNodeStatefulSetManifest()
		if err != nil {
			k.logger.Error(err, "failed to create inference statefulset manifest")
			return ctrl.Result{}, err
		}
		if err := ctrl.SetControllerReference(k.InferenceSession, statefulset, k.reconciler.Scheme); err != nil {
			k.logger.Error(err, "failed to adopt inference statefulset")
			return ctrl.Result{}, err
		}

		headlessService, err := k.getMultiNodeHeadlessServiceManifest()
		if err != nil {
			k.logger.Error(err, "failed to create inference statefulset headlessService manifest")
			return ctrl.Result{}, err
		}
		if err := ctrl.SetControllerReference(k.InferenceSession, headlessService, k.reconciler.Scheme); err != nil {
			k.logger.Error(err, "failed to adopt inference headlessService")
			return ctrl.Result{}, err
		}

		err = k.reconciler.Create(k.ctx, headlessService)
		if client.IgnoreAlreadyExists(err) != nil {
			k.logger.Error(err, "failed to create inference headlessService", "headlessService", headlessService)
			return ctrl.Result{}, err
		}

		err = k.reconciler.Create(k.ctx, statefulset)
		if client.IgnoreAlreadyExists(err) != nil {
			k.logger.Error(err, "failed to create inference statefulset", "statefulset", statefulset)
			return ctrl.Result{}, err
		}
	} else {
		// Single-node configuration: just a Deployment

		deployment, err := k.getSingleNodeDeploymentManifest()
		if err != nil {
			k.logger.Error(err, "failed to create inference deployment manifest")
			return ctrl.Result{}, err
		}
		if err := ctrl.SetControllerReference(k.InferenceSession, deployment, k.reconciler.Scheme); err != nil {
			k.logger.Error(err, "failed to adopt inference deployment")
			return ctrl.Result{}, err
		}

		err = k.reconciler.Create(k.ctx, deployment)
		if client.IgnoreAlreadyExists(err) != nil {
			k.logger.Error(err, "failed to create inference deployment", "deployment", deployment)
			return ctrl.Result{}, err
		}
	}

	// All good
	return ctrl.Result{}, nil
}

// ---------------------------------------------------------------------------
// These remaining methods implement the public Kernel interface that will be
// used by the generic reconciler.

func (k *InferenceSessionReconcilerKernel) step(
	succeededJobs []*batchv1.Job,
	failedJobs []*batchv1.Job,
) (result ctrl.Result, err error) {
	var downloadFailed bool = false
	for _, job := range failedJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == k.ArtifactsDownload().Component {
			downloadFailed = true
		}
	}

	var downloadSucceeded bool = false
	for _, job := range succeededJobs {
		step := job.ObjectMeta.Labels["dyff.io/step"]
		if step == k.ArtifactsDownload().Component {
			downloadSucceeded = true
		}
	}

	if downloadSucceeded {
		return k.onDownloadSucceeded()
	} else if downloadFailed {
		return k.onDownloadFailed()
	} else {
		return k.onCreate()
	}
}

// If the workflow has reached a terminal Condition, return that Condition. The
// operator should clean up any resources and set the returned Condition in the
// workflow k8s manifest.
//
// Else, return nil. The operator should let the workflow continue running.
func (k *InferenceSessionReconcilerKernel) terminalCondition() *metav1.Condition {
	var conditions []metav1.Condition = k.InferenceSession.Status.Conditions
	var condition *metav1.Condition

	// If top-level terminal condition is already set, return it
	// Note that we don't check for StatusReady because that's a "terminal"
	// status in Dyff and sessions run until explicitly terminated.
	for _, conditionType := range []string{StatusError} {
		condition = k8sMeta.FindStatusCondition(conditions, conditionType)
		if condition != nil && condition.Status == metav1.ConditionTrue {
			return condition
		}
	}

	if k.needsDownloadStep() {
		condition = k8sMeta.FindStatusCondition(conditions, InferenceSessionStatusDownloadSuccessful())
		if condition == nil || condition.Status == metav1.ConditionUnknown {
			return nil
		} else if condition.Status == metav1.ConditionFalse {
			return &metav1.Condition{
				Type:               StatusError,
				Status:             metav1.ConditionTrue,
				Reason:             "DownloadStepFailed",
				LastTransitionTime: metav1.NewTime(time.Now()),
				Message:            "The Download step Failed.",
			}
		}
	}

	// No errors -> keep running
	return nil
}

func (k *InferenceSessionReconcilerKernel) cleanup() (ctrl.Result, error) {
	deployment, err := k.getSingleNodeDeploymentManifest()
	if err != nil {
		deployment = nil
	}
	objects := []client.Object{
		k.getArtifactsDownloadJobManifest(),
		k.getArtifactsPersistentVolumeClaimManifest(),
		k.getServiceManifest(),
		deployment,
	}

	for _, manifest := range objects {
		if manifest != nil {
			err := k.reconciler.Delete(k.ctx, manifest, client.PropagationPolicy("Background"))
			if client.IgnoreNotFound(err) != nil {
				return ctrl.Result{}, err
			}
		}
	}
	return ctrl.Result{}, nil
}

//+kubebuilder:rbac:groups=dyff.io,resources=inferencesessions,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=dyff.io,resources=inferencesessions/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=dyff.io,resources=inferencesessions/finalizers,verbs=update
// TODO: SECURITY Figure out what the minimum necessary privileges are
//+kubebuilder:rbac:groups=core,resources=configmaps;services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps,resources=deployments;deployments/scale;statefulsets,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the InferenceSession object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *InferenceSessionReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var logger = log.FromContext(ctx)
	var err error

	var inferencesession dyffv1alpha1.InferenceSession
	err = r.Get(ctx, req.NamespacedName, &inferencesession)
	if err != nil {
		if k8sErrors.IsNotFound(err) {
			logger.V(1).Info("InferenceSession NotFound")
			return ctrl.Result{}, nil
		}
		logger.Error(err, "failed to get InferenceSession")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	logger.V(1).Info(fmt.Sprintf("reconcile %v", inferencesession.Spec.Id))

	kernel := r.newKernel(ctx, &inferencesession)

	// If in a terminal condition, set condition, clean up resources, return
	if terminal := kernel.terminalCondition(); terminal != nil {
		logger.Info("terminal status", "status", terminal)

		k8sMeta.SetStatusCondition(&kernel.InferenceSession.Status.Conditions, *terminal)
		err = r.Status().Update(ctx, kernel.InferenceSession)
		if err != nil {
			return ctrl.Result{}, err
		}

		return kernel.cleanup()
	}

	// Get child Jobs
	var childJobs batchv1.JobList
	err = r.List(ctx, &childJobs, client.InNamespace(req.Namespace), client.MatchingFields{r.ownerKey: req.Name})
	if err != nil {
		logger.Error(err, "failed to list child Jobs")
		return ctrl.Result{}, err
	}

	var failedJobs []*batchv1.Job
	var succeededJobs []*batchv1.Job
	var pendingJobs []*batchv1.Job
	for i, job := range childJobs.Items {
		finished, result := isJobFinished(&job)
		if finished {
			if result == batchv1.JobFailed {
				failedJobs = append(failedJobs, &childJobs.Items[i])
			} else if result == batchv1.JobComplete {
				succeededJobs = append(succeededJobs, &childJobs.Items[i])
			}
		} else {
			pendingJobs = append(pendingJobs, &childJobs.Items[i])
		}
	}

	// Pending job => wait for completion
	if len(pendingJobs) > 0 {
		logger.V(1).Info("waiting for pending jobs", "Jobs", jobNames(pendingJobs))
		return ctrl.Result{}, nil
	}

	return kernel.step(succeededJobs, failedJobs)
}

// SetupWithManager sets up the controller with the Manager.
func (r *InferenceSessionReconciler) SetupWithManager(mgr ctrl.Manager) error {
	r.ownerKey = ownerKeyPrefix + "." + strings.ToLower(kinds.InferenceSession)

	// Cache Jobs whose owner is an InferenceSession
	err := mgr.GetFieldIndexer().IndexField(
		context.Background(), &batchv1.Job{}, r.ownerKey,
		func(rawObj client.Object) []string {
			job := rawObj.(*batchv1.Job)
			owner := metav1.GetControllerOf(job)
			if owner == nil {
				return nil
			}
			if owner.Kind != kinds.InferenceSession {
				return nil
			}
			return []string{owner.Name}
		},
	)
	if err != nil {
		return err
	}

	workflowPredicate, err := predicate.LabelSelectorPredicate(metav1.LabelSelector{
		MatchLabels: map[string]string{
			"dyff.io/workflow": InferenceSessionWorkflowPlural(),
		},
	})
	if err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&dyffv1alpha1.InferenceSession{}).
		Owns(&appsv1.Deployment{}, builder.WithPredicates(workflowPredicate)).
		Owns(&appsv1.StatefulSet{}, builder.WithPredicates(workflowPredicate)).
		Owns(&batchv1.Job{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.PersistentVolumeClaim{}, builder.WithPredicates(workflowPredicate)).
		Owns(&corev1.Service{}, builder.WithPredicates(workflowPredicate)).
		Complete(r)
}
